# CrowdGrammar on Google Appengine

Gather native speakers suggestions on your essays -- cheap, quick and anonymous.

* Download the code: `git clone https://xuehuichao@bitbucket.org/xuehuichao/mturk-revisor.git`
* Running the code locally: `fab run`, then open your browser at `localhost:12080`
* Running tests: `fab test`.
* Production logs: `https://goo.gl/CavVIU`
* Production admin page: `http://www.crowdgrammar.com/admin`
* Release Instructions at https://goo.gl/KS09Pp .
* Updating Amazon account, (e.g. setting SQS queue, setting layout, getting hit types):

		python tools/config_aws.py
		
  The up-to-date configurations will show up in `bp_content/themes/default/config/aws_settings.json`.
