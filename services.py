import sys
import re
import os
import datetime
import logging
import json
from google.appengine.ext import ndb

from bp_includes.external_wrap.nltk_packages import sent_tokenize, word_tokenize
from bp_content.themes.default import rendering_data

from bp_includes.lib.basehandler import BaseHandler
from webapp2_extras.routes import RedirectRoute


class JsonRPCHandler(BaseHandler):

  def post(self):
    args = json.loads(self.request.body)
    result = self._Call(**args)
    self.response.headers['Content-Type'] = 'application/json'
    self.response.write(json.dumps(result))

  def _Call(self, **args):
    raise NotImplementedError


class RpcRoutes(object):
  """Registering RPC routes /_api/{method_name}

  When using it, first declare a routes variable, then use for decorators:

  routes = RpcRoutes()

  ...

  @routes.RegisterRPC("method_name")
  class XXXHandler(JsonRPCHandler):
    ...
  """

  def __init__(self):
    self.secure_scheme = 'https'
    self._routes = []

  def get_routes(self):
    return self._routes

  def add_routes(self, app):
    if app.debug:
      self.secure_scheme = 'http'
    for r in self._routes:
      app.router.add(r)

  def RegisterRPC(self, method_name):
    """Will register /_api/{method_name} as the method.
    """
    def _Wrappee(handler_class):
      self._routes.append(RedirectRoute(
        '/_api/%s' % method_name,
        handler_class,
        name=method_name,
        strict_slash=True))
      return handler_class

    return _Wrappee

routes = RpcRoutes()

# It should work with the following command (uncommented):
# curl -H "Content-Type: application/json"  -X POST -d '{"content": "I
# think this is super cool. Dont you think so?"}'
# http://sword.xuehuichao.com/_api/preprocess && echo


@routes.RegisterRPC("preprocess")
class PreprocessContentHandler(JsonRPCHandler):

  def _Call(self, content):
    paragraph_contents = re.split(r'\s*\n\s*\n\s*', content)
    paragraphs = []
    for paragraph_content in paragraph_contents:
      new_sent_list = []
      sentence_list = sent_tokenize(paragraph_content)
      logging.info("%r",sentence_list)
      if len(sentence_list) == 1:
        for sent_surface in sentence_list:
          """words = word_tokenize(sent_surface)"""
          pre_sentence = ""
          next_sentence = ""
          context = {'words': sent_surface, 'previous_sentence': pre_sentence,
                       'next_sentence': next_sentence}
          new_sent_list.append(context)
      else:
        for index in range(0,len(sentence_list)):
          if index == 0:
            pre_sentence = ""
            next_sentence = sentence_list[index+1]
            context = {'words': sentence_list[index], 'previous_sentence': pre_sentence,
                       'next_sentence': next_sentence}
            new_sent_list.append(context)
          elif index == len(sentence_list)-1:
            next_sentence = ""
            pre_sentence = sentence_list[index-1]
            context = {'words': sentence_list[index], 'previous_sentence': pre_sentence,
                       'next_sentence': next_sentence}
            new_sent_list.append(context)
          else:
            pre_sentence = sentence_list[index-1]
            next_senence = sentence_list[index+1]
            context = {'words': sentence_list[index], 'previous_sentence': pre_sentence,
                       'next_sentence': next_sentence}
            new_sent_list.append(context)
      paragraphs.append({'sentences': new_sent_list})
    result = {"paragraphs": paragraphs}
    return result


@routes.RegisterRPC("get_progress")
class GetArticleProgressHandler(JsonRPCHandler):

  def _Call(self, article_urlsafe):
    article = ndb.Key(urlsafe=article_urlsafe).get()
    response = {
      "n_finished": article.GetNFinished(),
      "n_total": article.GetNRequested()}
    return response


@routes.RegisterRPC("get_result")
class GetRevisionResultHandler(JsonRPCHandler):

  def _Call(self, article_urlsafe):
    logging.info("Fetching revision result for %s", article_urlsafe)
    article = ndb.Key(urlsafe=article_urlsafe).get()
    rendering_dict = article.GetRenderingData().PutIntoDict()

    return rendering_dict


@routes.RegisterRPC("get_user_info")
class GetUserInfoHandler(JsonRPCHandler):

  def _Call(self, user_urlsafe):
    user = ndb.Key(urlsafe=user_urlsafe).get()
    response = {"credits": user.credits}
    return response
