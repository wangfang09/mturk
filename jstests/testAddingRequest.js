describe('AddRequestCtrl', function() {
  beforeEach(module('AddRequestApp'));

  var $controller;

  beforeEach(inject(function(_$controller_) {
    // The injector unwraps the underscores (_) from around the parameter names when matching
    $controller = _$controller_;
  }));

  describe('UpdatingSelections', function() {
    var $scope, controller;

    beforeEach(function() {
      var customized_resp = {
        "paragraphs": [{
          "sentences": [{
            "words": ["Sentence", "one", "has", "six", "words", "."]
          }, {
            "words": ["Sentence", "two", "has", "five", "."]
          }, ]
        }, {
          "sentences": [{
            "words": ["Sentence", "three", "has", "some", "more", "words", "."]
          }, {
            "words": ["Sentence", "four", "does", "not", "."]
          }, ]
        }, ],
      };

      $scope = {
        '$apply': function(func) {
          func();
        },
      };
      controller = $controller(
        'AddRequestCtrl', {
          $scope: $scope,
          'WordContentPreprocessing': {
            'Preproc': function(content, call_back) {
              call_back({"data" : customized_resp});
            },
            'GetUserInfo': function(urlsafe, call_back) {
              call_back({"data" : { "credits": 200, }});
            }
          },
          'ArgumentLoading': {
            'GetUserURLSafe': function() {
              return 'user_urlsafe';
            },
          }
        });
      $scope.content = "Sentence one has six words. Sentence two has five.\n\nSentence three has some more words. Sentence four does not."
      $scope.init_func();
    });

    it('Initially all sentences are below activation threshold', function() {
      $scope.ProceedToSecondStep(function() {
        expect(JSON.parse($scope.request)).toEqual([]);
      });
    });

    it('Updates activation list when changing minimum sentnece length', function() {
      $scope.ProceedToSecondStep(function() {
        $scope.min_sent_len = 0;
        $scope.UpdateActivations();
        expect(JSON.parse($scope.request)).toEqual(['0', '1', '2', '3']);
      });
    });

    it('Toggling selection changes activation list', function() {
      $scope.ProceedToSecondStep(function() {
        $scope.min_sent_len = 0;
        $scope.UpdateActivations();

        $scope.ToggleActivation(0);

        expect(JSON.parse($scope.request)).toEqual(['1', '2', '3']);
      });
    });

    it('Displays the right cost', function() {
      $scope.ProceedToSecondStep(function() {
        $scope.min_sent_len = 0;
        $scope.UpdateActivations();

        expect($scope.current_credits).toEqual(200);
        // Basic's cost * 4 sentences
        expect($scope.total_credits).toEqual(3 * 4);
        expect($scope.credits_after).toEqual(200 - 3 * 4);
      });
    });

  });
});
