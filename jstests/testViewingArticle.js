describe('ViewArticleCtrl', function() {
  beforeEach(module('ViewArticleApp'));

  var $controller;

  beforeEach(inject(function(_$controller_) {
    // The injector unwraps the underscores (_) from around the parameter names when matching
    $controller = _$controller_;
  }));

  describe('ViewingArticleProgress', function() {
    var $scope, controller;

    beforeEach(function() {
      var customized_resp = {
        "title": "In this test, I am p...",
        "url": "/article/my_urlsafe",
        "paypal_url": "/paypal/my_urlsafe",
        "price": 1.0,
        "archive_url": "/article/my_urlsafe",
        "paragraphs": [{
          "sentences": [{
            "annotation_spans": [{
              "revise_into": "",
              "original_words": "In this test,"
            }, {
              "original_words": "I am planning to first submit corrections for one sentence, and then two sentences."
            }],
            "finished_annotation": true,
            "annotation_request_pending": false
          }, {
            "annotation_spans": [{
              "original_words": "Also I will test tracking progress for non-existing sentences."
            }],
            "finished_annotation": false,
            "annotation_request_pending": true
          }]
        }],
        "payment_pending": false
      }

      $scope = {
        '$apply': function(func) {
          func();
        },
      };

      controller = $controller(
        'ViewArticleCtrl', {
          $scope: $scope,
          'ViewArticleDataFetching': {
            'GetRevisionResult': function(article_urlsafe, call_back) {
              call_back({"data" : customized_resp});
            },
	    'GetArticleURLSafe': function() {
	      return "my_urlsafe";
	    }
          },
        });
      $scope.init_func();
      $scope.content = "I am planning to first submit corrections for one sentence, and then two sentences. Also I will test tracking progress for non-existing sentences."
    });

    it('Works -- empty', function() {});


  });
});
