"""Setting up AWS (SQS, Mturk) and store the settings at the json config file.

Usage:
  Running python tools/config_aws.py directly. It will contact AWS server and
  refresh the aws_settings.json file.

  - Setting up SQS to receive messages upon assignment submissions
  - Result will show up at bp_content/themes/default/config/aws_settings.json.
"""

# TODO(xuehuichao): move all the SQS related stuffs from current
# settings.py into sqs_settings.py

import json
import sys
import logging
from datetime import timedelta
from boto.mturk.connection import MTurkConnection
from boto.mturk.qualification import LocaleRequirement
from boto.mturk.qualification import NumberHitsApprovedRequirement
from boto.mturk.qualification import PercentAssignmentsApprovedRequirement
from boto.mturk.qualification import Qualifications
from boto.mturk.layoutparam import LayoutParameter
from boto.mturk.layoutparam import LayoutParameters


OUTPUT_JSON_LOC = "bp_content/themes/default/config/aws_settings.json"

# Amazon related settings
AWS_ACCOUNTS = {
  "xhc": {
    "ACCESS_ID": 'AKIAIAEQIDOQZD4ZTAIA',
    "SECRET_KEY": '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',
    "AWS_PAR": {
      "prod": {
        "LAYOUT_ID_MAP": {"original": '314E9ZG2QVY41RVWYQ1YQ8UI22UPEK'},
        "SQS_PATH": 'https://sqs.us-east-1.amazonaws.com/682770784995/turk',
      },
      
    },
  },

  "hljz": {
    "ACCESS_ID": 'AKIAJMA33AADMWD3XH6A',
    "SECRET_KEY": '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',
    "AWS_PAR": {
      "prod": {
        "LAYOUT_ID_MAP": {"original": '3FJ1DDZQJBLV8ZAG6EXIZEHM9N3PGG'},
        "SQS_PATH": 'https://sqs.us-west-2.amazonaws.com/151838343339/turk',
      },
    },
  },
  
  "wf":{
    "ACCESS_ID": 'AKIAI2UOT5E2YGIGLGNQ',
    "SECRET_KEY": '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',
    "AWS_PAR": {
      "prod" :{
        "LAYOUT_ID_MAP": {"original": '3P675GGUDKW27HICYJ3H8FV5V35WFG'},
        "SQS_PATH": 'https://sqs.us-east-1.amazonaws.com/520621516669/turk',
          },
      
      "sandbox":{
        "LAYOUT_ID_MAP": {"original": '30ASDUGRUGZFJ9XASY84ECTG32PATY'},
        "SQS_PATH": 'https://sqs.us-east-1.amazonaws.com/520621516669/turk_sandbox',
          },
        },
    },
}

HOST_MAP = {"prod": 'mechanicalturk.amazonaws.com',
            "sandbox": "mechanicalturk.sandbox.amazonaws.com"}


# Running configurations
amt_settings = {
  "appengine": {                # used for hosting in appengine
    "amazon_user": "xhc",
    "host_type": "prod",
    "layout": "original",
    "sqs_name": "turk",
    "no_turk_qualifications": False,
    "aws_region": 'us-east-1',
  },
  "prod": {                     # used for hosting in China
    "amazon_user": "wf",
    "host_type": "prod",
    "layout": "original",
    "sqs_name": "turk",
    "no_turk_qualifications": False,
    "aws_region": 'us-east-1',
  },
  "dev": {                      # used for hosting locally
    "amazon_user": "wf",
    "host_type": "sandbox",
    "layout": "original",
    "sqs_name": "turk_sandbox",
    "no_turk_qualifications": False,
    "aws_region": 'us-east-1',
  },
}


def main():
  logging.basicConfig(stream=sys.stdout, level=logging.INFO)

  config_map = {}

  for setting_name, settings in amt_settings.iteritems():
    logging.info("Generating settings for %s", setting_name)
    amazon_user = settings["amazon_user"]
    host_type = settings["host_type"]

    config_map[setting_name] = {
      "cent_hittype_map": {},
      "aws_access_key_id": AWS_ACCOUNTS[amazon_user]["ACCESS_ID"],
      "aws_secret_access_key": AWS_ACCOUNTS[
        amazon_user]["SECRET_KEY"],
      "no_turk_qualifications": settings["no_turk_qualifications"],
      "layout_id": AWS_ACCOUNTS[amazon_user]["AWS_PAR"][host_type]["LAYOUT_ID_MAP"][settings["layout"]],
      "sqs_name": settings["sqs_name"],
      "aws_region": settings["aws_region"],
      "sqs_path": AWS_ACCOUNTS[amazon_user]["AWS_PAR"][host_type]["SQS_PATH"],
      "mturk_host": HOST_MAP[host_type], }

    mtc = MTurkConnection(aws_access_key_id=config_map[setting_name]["aws_access_key_id"],
                          aws_secret_access_key=config_map[
                            setting_name]["aws_secret_access_key"],
                          host=config_map[setting_name]["mturk_host"])

    quals = Qualifications()
    quals.add(LocaleRequirement("EqualTo", "US"))
    quals.add(NumberHitsApprovedRequirement("GreaterThanOrEqualTo", 10))
    quals.add(
      PercentAssignmentsApprovedRequirement(
        "GreaterThanOrEqualTo", 95))
    """quals = None"""

    description = ('We have sentences written by a non - native English speakers. '
                   'Please make suggestions so that the sentence reads better.')

    for cents in range(1, 11):
      hit_type = mtc.register_hit_type(
        title='Make the sentence sound more native',
        description=description,
        reward=0.01 * cents,
        keywords=['English', 'revision', 'grammar errors'],
        duration=timedelta(hours=1),
        approval_delay=timedelta(days=1),
        qual_req=quals,
      )[0]
      config_map[setting_name]["cent_hittype_map"][
        cents] = hit_type.HITTypeId
      mtc.set_sqs_notification(
        hit_type.HITTypeId,
        config_map[setting_name]['sqs_path'],
        'HITReviewable')
      logging.info(
        "Set SQS notification for %s (%d cents)",
        setting_name,
        cents)

  with open(OUTPUT_JSON_LOC, "w") as ofile:
    ofile.write(
      json.dumps(
        config_map,
        sort_keys=True,
        indent=2,
        separators=(
          ',',
          ': ')))
  logging.info("Output successfully written to %s", OUTPUT_JSON_LOC)


if __name__ == "__main__":
  main()
