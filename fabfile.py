"""
    REQUIREMENTS:
        - install pip with distribute (http://packages.python.org/distribute/)
        - sudo pip install Fabric

"""

import threading
import subprocess
import time
import urllib
import logging
import os
from fabric.api import local


def lang(mode="extract"):
  """
      REQUIREMENTS:

      - Install before pip with distribute_setup.py (Read the environment setup document)
      - sudo pip install babel
      - sudo pip install jinja2

      HOW TO RUN:

          option 1) fab lang
          option 2) fab lang:compile
  """

  if mode == "compile":
    local("pybabel compile -f -d ./locale")
  else:
    local("pybabel extract -F ./locale/babel.cfg -o ./locale/messages.pot ./ --sort-output --no-location --omit-header")
    local("pybabel update -l en_US -d ./locale -i ./locale/messages.pot --previous --ignore-obsolete")
    local("pybabel update -l zh_CN -d ./locale -i ./locale/messages.pot --previous --ignore-obsolete")


class CronTaskCaller(threading.Thread):

  def __init__(self, address, every_x_seconds=60):
    threading.Thread.__init__(self)
    self._address = address
    self._every_x_seconds = every_x_seconds

  def run(self):
    while True:
      time.sleep(self._every_x_seconds)
      try:
        urllib.urlopen(self._address).read()
      except Exception as e:
        logging.error("Error executing cron task %r: %s", self._address, e)


def start(mode="normal"):
  """
      HOW TO RUN:

          option 1) fab start
          option 2) fab start:clear

  """

  CronTaskCaller(
    "http://sword.xuehuichao.com/admin/crontasks/update_sqs").start()

  # Getting the dev_appserver.py address
  stdout, stderr = subprocess.Popen(
    'which dev_appserver.py', shell=True, stdout=subprocess.PIPE,
    stderr=subprocess.PIPE).communicate()
  dev_appserver_addr = stdout.strip()
  if mode == "clear" or mode == "clean":
    local("sudo {} ./ --host sword.xuehuichao.com --port 80 --admin_host 0.0.0.0 --admin_port 8000 --clear_datastore=yes".format(dev_appserver_addr))
  else:
    local("sudo {} ./ --host sword.xuehuichao.com --port 80 --admin_host 0.0.0.0 --admin_port 8000".format(dev_appserver_addr))


def deploy():
  """
      app.yaml never has to be version:default

  """

  local("appcfg.py --oauth2 update .")


def test(target="all", verbose_mode="normal"):
  """
  fab test
  OR fab test:target_name

  """

  local(
    "theme=default python runtests.py {0} {1}".format(
      target,
      verbose_mode))


def jstest():
  """
  fab jstest
  """

  local("node_modules/karma/bin/karma start")


def nlp(mode="build"):
  """fab nlp
  OR fab nlp:run
  """
  if mode == "build":
    local("cd nlp_server && docker build -t nlp_server .")
  elif mode == "run":
    local("docker run -d -p 12085:8085 nlp_server")
  elif mode == "deploy":
    local("docker run -d -p 8085:8085 nlp_server")
  elif mode == 'stop':
    local("docker stop $(docker ps -a -q --filter ancestor=nlp_server )")


def china():
  """Deploying to China.
  """

  dev_dir = os.path.dirname(os.path.realpath(__file__))
  appscale_dir = os.path.join(dev_dir, "china")
  subprocess.Popen(["appscale", "deploy", dev_dir], cwd=appscale_dir).wait()
