import logging
import service
import unittest


class TestExtractingPDF(unittest.TestCase):

  def setUp(self):
    self.example_pdf_path = "pdf_examples/eacl2014.pdf"

  def tearDown(self):
    pass

  def testExtractingFromPDFContent(self):
    result = service.ExtractText(
      {"pdf_content": open(self.example_pdf_path).read()})
    self.assertIn("This paper investigates", result["text"])
