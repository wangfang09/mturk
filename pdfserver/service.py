import json
import tempfile
import subprocess

from werkzeug.wrappers import Request, Response
from werkzeug.serving import run_simple
from jsonrpc import JSONRPCResponseManager, dispatcher


@dispatcher.add_method
def ExtractText(extract_pdf_request):
  content = extract_pdf_request['pdf_content']
  pdf_file = tempfile.NamedTemporaryFile(suffix=".pdf")
  pdf_file.write(content)
  pdf_file.flush()

  txt_file = tempfile.NamedTemporaryFile(suffix=".txt")
  subprocess.Popen(["pdf2txt.py", pdf_file.name], stdout=txt_file).wait()

  return {'text': open(txt_file.name).read()}


@Request.application
def application(request):
  # Dispatcher is dictionary {<method_name>: callable}
  response = JSONRPCResponseManager.handle(
    request.data, dispatcher)
  rv = Response(response.json, mimetype='application/json')

  rv.headers.add('Access-Control-Allow-Origin', '*')
  rv.headers.add('Access-Control-Allow-Methods',
                 'GET,PUT,POST,DELETE,PATCH')
  rv.headers.add('Access-Control-Allow-Headers',
                 'Content-Type, Authorization')

  return rv


def main():
  run_simple('0.0.0.0', 8085, application)

if __name__ == '__main__':
  main()
