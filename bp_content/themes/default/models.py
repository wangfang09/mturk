# Put here your models or extend User model from bp_includes/models.py

import settings
import logging
import json
import re
import sequences
import rendering_data
from google.appengine.ext import ndb
from bp_includes import models as bp_models
from google.appengine.api import taskqueue
from google.appengine.api import users
from google.appengine.api import memcache
from bp_includes.external_wrap.nltk_packages import sent_tokenize
from bp_includes.external_wrap.nltk_packages import word_tokenize


class SentenceRevisionSuggestion(ndb.Model):
  start = ndb.IntegerProperty()
  end = ndb.IntegerProperty()
  revise_into = ndb.StringProperty()
  

class SentenceAnnotationRequest(ndb.Model):
  result = ndb.LocalStructuredProperty(SentenceRevisionSuggestion,
                                       repeated=True)
  # PAID, SUBMITTED, DONE, SUBMISSION_ERROR
  phase = ndb.StringProperty()
  hit_id = ndb.StringProperty()
  price_in_cents = ndb.IntegerProperty()

  @staticmethod
  def GetOpenRequests():
    return list(SentenceAnnotationRequest.query(SentenceAnnotationRequest.phase
                                                == 'PAID').iter())

  def GetAnnotateURL(self):
    return '/annotation/' + self.key.urlsafe()

  def GetSentence(self):
    return self.key.parent().get()

  def SetSuggestions(self, suggestions):
    self.result = [
      SentenceRevisionSuggestion(start=s,
                                 end=e,
                                 revise_into=t) for s, e, t in suggestions
    ]
    self.phase = 'DONE'
    ndb.put_multi(self.result)
    self.put()

  def GetPriceInCents(self):
    return self.price_in_cents


class Sentence(ndb.Model):
  surface = ndb.StringProperty()
  words = ndb.StringProperty(repeated=True)
  annotation_request_key = ndb.KeyProperty(SentenceAnnotationRequest)
  out_for_annotations = ndb.BooleanProperty()
  previous_sentence = ndb.StringProperty()
  next_sentence = ndb.StringProperty()

  def GetSnippet(self):
    return ' '.join(self.words)

  def ToJson(self):
    return json.dumps(self.words)

  def GetSuggestions(self):
    if self.annotation_request_key is None:
      return []
    suggestions = []
    for sug in self.annotation_request_key.get().result:
      suggestions.append((sug.start, sug.end, sug.revise_into))
    return suggestions


class Paragraph(ndb.Model):
  annot_req_key = ndb.KeyProperty("AnnotationRequest")
  sentence_inds = ndb.IntegerProperty(repeated=True)

  def GetAnnotationRequest(self):
    return self.annot_req_key.get()

  def GetSentences(self):
    all_sentences = self.GetAnnotationRequest().GetSentences()
    return [all_sentences[i] for i in self.sentence_inds]


class Order(ndb.Model):
  """An order by the customer.

  It contains the user, payment info, as well specifications of commodities.
  Specifications are stored in the subclasses. Subclasses need to define
  DoMarkAsPaid, for the processing of order fulfillment.
  """
  creation_timestamp = ndb.DateTimeProperty(auto_now=True)
  user = ndb.KeyProperty("User")
  amount = ndb.FloatProperty()  # Dollar value of the full request.
  stage = ndb.StringProperty()  # CREATED, PAID, SUBMITTED, ABORTED,
  # SUBMISSION_ERROR

  def MarkAsPaid(self):
    if self.stage in ['PAID', 'SUBMITTED']:
      logging.error('Duplicated payment notifications: %s',
                    self.key.urlsafe())
      return

    self.stage = 'PAID'
    self.put()
    try:
      self.DoMarkAsPaid()
    except Exception as e:
      logging.error("Error in submitting Order %s. %s.",
                    self.key.urlsafe(), e)
      self.stage = "SUBMISSION_ERROR"
    else:
      self.stage = 'SUBMITTED'
    self.put()

  def DoMarkAsPaid(self):
    raise NotImplementedError()

  def PaymentPending(self):
    return self.stage == 'CREATED'

  def GetURL(self):
    return ""

  def GetItemDescription(self):
    return "Article revising service"

  def RenderPaypalRedirect(self, handler):
    handler.Render('order_placement_redirect.html', {
      'paypal_url': settings.PAYPAL_URL,
      'amount': self.amount,
      'order_completion_url': self.GetURL(),
      'order_key_urlsafe': self.key.urlsafe(),
      'paypal_business_id': settings.PAYPAL_BUSINESS_ID,
      'item_name': self.GetItemDescription(),
    })


class AnnotationRequest(Order):

  title = ndb.StringProperty(indexed=False)
  content = ndb.StringProperty(indexed=False)
  archived = ndb.BooleanProperty(default=False)
  preprocessed = ndb.BooleanProperty(default=False)
  creation_timestamp = ndb.DateTimeProperty(auto_now=True)
  paragraphs = ndb.LocalStructuredProperty(Paragraph, repeated=True)

  sentences_ind_list = ndb.IntegerProperty(repeated=True)
  unit_price = ndb.FloatProperty()  # DEPRECATED
  price_choice = ndb.StringProperty()  # BASIC, PRIORITY, PREMIUM

  sentences_key_list = ndb.KeyProperty(Sentence, repeated=True)
  # Precalculated at queries.
  known_have_finished = ndb.IntegerProperty(repeated=True)

  def GetRenderingData(self):
    art_data = rendering_data.ArticleDisplayData(
      self.GetTitle(), self.GetURL(), self.GetPrice(),
      self.GetPaypalURL(), self.GetArchiveUrl(),
      self.PaymentPending(),
    )

    all_sentences = self.GetSentences()
    avail_annot = ndb.get_multi(
      s.annotation_request_key for s in all_sentences if s.annotation_request_key)
    iter_annot = iter(avail_annot)
    all_annotations = []
    for s in all_sentences:
      if s.annotation_request_key:
        all_annotations.append(iter_annot.next())
      else:
        all_annotations.append(None)

    for par in self.paragraphs:
      par_data = []
      for sent_ind in par.sentence_inds:
        par_data.append(rendering_data.SentenceDisplayData(
          all_sentences[sent_ind], all_annotations[sent_ind]))
      art_data.AddParagraph(par_data)
    return art_data

  def GetRenderingMessage(self):
    return self.GetRenderingData().PutIntoMessage()

  def __iter__(self):
    if self.stage != 'SUBMITTED':
      return
    for sid in self.sentences_ind_list:
      yield self.GetSentenceByInd(sid).annotation_request_key.get()

  def GetUnitPriceInCents(self):
    return int(round(settings.TURKER_COST_DOLLARS[self.price_choice] * 100))

  def GetAuthor(self):
    return self.key.parent().get()

  def GetPrice(self):
    return self.amount

  def GetNRequested(self):
    return len(self.sentences_ind_list)

  def GetNFinished(self):
    return self.GetRenderingData().GetNFinished()

  def GetURLSafeKey(self):
    return self.key.urlsafe()

  def DoMarkAsPaid(self):
    self.GenerateSentenceRequests()

  def GenerateSentenceRequests(self):

    sent_annot_requests = []
    cents = self.GetUnitPriceInCents()
    sents = []
    queue = taskqueue.Queue()
    rpc_list = []
    for sent in self.IterRequestedSentences():
      req = SentenceAnnotationRequest(phase='PAID',
                                      parent=sent.key,
                                      price_in_cents=cents)
      sent.annotation_request_key = req.put()
      sents.append(sent)
      rpc = taskqueue.create_rpc()
      task = taskqueue.Task(
        url='/taskqueue_submit_turker_request',
        params={'request_key': sent.annotation_request_key.urlsafe()})
      queue.add_async(task, rpc=rpc)
      rpc_list.append(rpc)
    ndb.put_multi(sents)
    memcache.delete(self.GetSentencesMemcacheKey())

    for rpc in rpc_list:
      rpc.check_success()

  def GetParagraphs(self):
    return self.paragraphs

  def IterRequestedSentences(self):
    for sid in self.sentences_ind_list:
      yield self.GetSentenceByInd(sid)

  def GetSentencesMemcacheKey(self):
    return "{}|sentences".format(self.key.urlsafe())

  def GetSentences(self):
    mem_key = self.GetSentencesMemcacheKey()
    sentences = memcache.get(mem_key)
    if sentences is None:
      sentences = ndb.get_multi(self.sentences_key_list)
      memcache.add(mem_key, sentences)

    return sentences

  def GetNSentences(self):
    return len(self.sentences_key_list)

  def GetSentencesByInds(self, inds):
    sentences = self.GetSentences()
    return [sentences[i] for i in inds]

  def GetSentenceByInd(self, ind):
    return self.GetSentencesByInds([ind])[0]

  def GetTitle(self):
    return self.title

  def GetURL(self):
    return '/article/%s' % self.key.urlsafe()

  def GetArchiveUrl(self):
    return '/archive/%s' % self.key.urlsafe()

  def GetPaypalURL(self):
    return '/paypal/%s' % self.key.urlsafe()

  def Archive(self):
    self.archived = True
    self.put()

  def CancelIssuedRequests(self):
    for sent in self.GetSentences():
      req_key = sent.annotation_request_key
      taskqueue.add(
        url='/taskqueue_expire_hit/',
        params={
          'req_urlsafe': req_key.urlsafe()})
    self.stage = 'ABORTED'
    self.put()


class User(bp_models.User):

  annotation_requests = ndb.KeyProperty(AnnotationRequest, repeated=True)
  credits = ndb.IntegerProperty(default=settings.INITIAL_USER_CREDITS)

  @staticmethod
  def GetCurrentUser(handler):
    if handler.user:
      user_info = handler.user_model.get_by_id(long(handler.user_id))
      return user_info

  def GetCredits(self):
    return self.credits

  def GetNickname(self):
    if self.name or self.last_name:
      return self.name + " " + self.last_name
    else:
      return self.email

  def _GetSnippetText(self, content):
    snippet = content[:settings.SNIPPET_LENG]
    if len(content) > settings.SNIPPET_LENG:
      snippet += '...'
    return snippet

  def Activate(self):
    bp_models.User.Activate(self)
    if len(self.annotation_requests) == 1:
      self.annotation_requests[0].get().MarkAsPaid()

  def GetVisibleAnnotationRequests(self):
    return [i for i in ndb.get_multi(
      self.annotation_requests) if not i.archived]

  def PreprocessThenAddAnnotationRequest(self, content, title, sent_price):
    paragraph_contents = re.split(r'\s*\n\s*\n\s*', content)
    paragraphs = []
    sent_count = 0
    for paragraph_content in paragraph_contents:
      new_sent_list = []
      sentence_list = sent_tokenize(paragraph_content)
      if len(sentence_list) == 1:
        for sent_surface in sentence_list:
          """words = word_tokenize(sent_surface)"""
          pre_sentence = ""
          next_sentence = ""
          context = {'words': sentence_list[index], 'previous_sentence': pre_sentence,
                       'next_sentence': next_sentence}
          new_sent_list.append(context)
          """new_sent_list.append({'words': sent_surface})
          new_sent_list.append({'previous_sentence': pre_sentence})
          new_sent_list.append({'next_sentence': next_sentence})"""
          sent_count += 1
      else:
        for index in range(0,len(sentence_list)):
          if index == 0:
            pre_sentence = ""
            next_sentence = sentence_list[index+1]
            context = {'words': sentence_list[index], 'previous_sentence': pre_sentence,
                       'next_sentence': next_sentence}
            new_sent_list.append(context)
            sent_count += 1
          elif index == len(sentence_list)-1:
            next_sentence = ""
            pre_sentence = sentence_list[index-1]
            context = {'words': sentence_list[index], 'previous_sentence': pre_sentence,
                       'next_sentence': next_sentence}
            new_sent_list.append(context)
            sent_count += 1
          else:
            pre_sentence = sentence_list[index-1]
            next_senence = sentence_list[index+1]
            context = {'words': sentence_list[index], 'previous_sentence': pre_sentence,
                       'next_sentence': next_sentence}
            new_sent_list.append(context)
            sent_count += 1
      paragraphs.append({'sentences': new_sent_list})
    paragraphs_json = {'paragraphs': paragraphs}

    sent_ids = range(sent_count)
    return self.AddAnnotationRequest(
      content, title, paragraphs_json, sent_ids, sent_price)

  def AddAnnotationRequest(self, content, title, paragraphs_json, sent_ids,
                           price_choice):

    if title == '':
      title = self._GetSnippetText(content)

    annotation_request = AnnotationRequest(
      user=self.key,
      content=content,
      title=title,
      parent=self.key,
      sentences_ind_list=sent_ids,
      stage='CREATED',
      price_choice=price_choice)
    annot_req_key = annotation_request.put()

    sent_id_set = set(sent_ids)

    sentence_instances = []
    cur_sent_id = 0
    for par_json in paragraphs_json['paragraphs']:
      for sent_json in par_json['sentences']:
        processed_words = word_tokenize(sent_json['words'])
        logging.info("%r",sent_json['previous_sentence'])
        logging.info("%r",sent_json['next_sentence'])
        sent_inst = Sentence(surface = sent_json['words'],
                             words = processed_words,
                             parent=annot_req_key,
                             out_for_annotations=(cur_sent_id in
                                                  sent_id_set),
                             previous_sentence = sent_json['previous_sentence'],
                             next_sentence = sent_json['next_sentence'])
        cur_sent_id += 1
        sentence_instances.append(sent_inst)

    sentence_key = ndb.put_multi(sentence_instances)
    annotation_request.sentences_key_list = sentence_key

    ind_iter = iter(range(len(sentence_key)))
    for par_json in paragraphs_json['paragraphs']:
      ind_list = []
      for sent_json in par_json['sentences']:
        ind_list.append(ind_iter.next())
      new_par = Paragraph(annot_req_key=annot_req_key, sentence_inds=ind_list)
      annotation_request.paragraphs.append(new_par)

    annotation_request.put()
    self.annotation_requests.append(annot_req_key)
    self.put()
    return annotation_request


class InsufficientCreditsError(Exception):
  pass


class AddCreditsOrder(Order):
  n_credits = ndb.IntegerProperty()

  def DoMarkAsPaid(self):
    user = self.user.get()
    user.credits += self.n_credits
    logging.info("Added %d credits to account of %s",
                 self.n_credits, user)
    user.put()

  def GetItemDescription(self):
    return "%d sentence revision credits" % self.n_credits
