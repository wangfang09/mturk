"""Pricing calculation, and configuration utils.
"""


def OrderPriceForCredits(n_credits):
  return n_credits / 100.0 + 0.3
