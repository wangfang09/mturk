"""
Using redirect route instead of simple routes since it supports strict_slash
Simple route: http://webapp-improved.appspot.com/guide/routing.html#simple-routes
RedirectRoute: http://webapp-improved.appspot.com/api/webapp2_extras/routes.html#webapp2_extras.routes.RedirectRoute
"""
from webapp2_extras.routes import RedirectRoute
from bp_content.themes.default.handlers import handlers

secure_scheme = 'https'

# Here go your routes, you can overwrite boilerplate routes
# (bp_includes/routes)

_routes = [
  RedirectRoute(
    '/secure/',
    handlers.SecureRequestHandler,
    name='secure',
    strict_slash=True),
  RedirectRoute(
    '/settings/delete_account',
    handlers.DeleteAccountHandler,
    name='delete-account',
    strict_slash=True),
  RedirectRoute(
    '/contact/',
    handlers.ContactHandler,
    name='contact',
    strict_slash=True),
  RedirectRoute('/', handlers.MainPageHandler, name='main', strict_slash=True),
  RedirectRoute(
    '/intro',
    handlers.IntroPageHandler,
    name='intro',
    strict_slash=True),
  RedirectRoute(
    '/testdrive/',
    handlers.TestDriveHandler,
    name='testdrive',
    strict_slash=True),
  RedirectRoute(
    '/dashboard',
    handlers.DashboardHandler,
    name='dashboard',
    strict_slash=True),
  RedirectRoute(
    '/add_request/',
    handlers.AddRequestHandler,
    name='add_request',
    strict_slash=True),
  RedirectRoute(
    '/article/<article_key_url>',
    handlers.ShowArticleHandler,
    name='article',
    strict_slash=True),
  RedirectRoute(
    '/turk',
    handlers.ShowAllSentRequestsHandler,
    name='turk',
    strict_slash=True),
  RedirectRoute(
    '/annotation/<request_url_key>',
    handlers.AnnotationHandler,
    name='annotation',
    strict_slash=True),
  RedirectRoute(
    '/annotation',
    handlers.AnnotationHandler,
    name='annotation_post',
    strict_slash=True),
  RedirectRoute(
    '/paypal_ipn',
    handlers.PaypalIPNHandler,
    name='paypal_ipn',
    strict_slash=True),
  RedirectRoute(
    '/taskqueue_submit_turker_request',
    handlers.SubmitTurkerRequestHandler,
    name='taskqueue_submit_turker_request',
    strict_slash=True),
  RedirectRoute(
    '/admin/crontasks/update_sqs',
    handlers.UpdateSQSHandler,
    name='update_sqs',
    strict_slash=True),
  RedirectRoute(
    '/taskqueue_expire_hit/',
    handlers.ExpireSentReqHanlder,
    name='expire_sent',
    strict_slash=True),
  RedirectRoute(
    '/markaspaid/<order_urlsafe>',
    handlers.MarkAsPaidHandler,
    name='markaspaid',
    strict_slash=True),
  RedirectRoute('/taskqueue_mark_order_as_paid/',
                handlers.MarkAsPaidHandler, name='taskqueue_mark_order_as_paid', strict_slash=True),
  RedirectRoute(
    '/paypal/<annotation_request_urlsafe>',
    handlers.PaypalHandler,
    name='paypal',
    strict_slash=True),
  RedirectRoute(
    '/archive/<article_key_url>',
    handlers.ArchiveHandler,
    name='archive',
    strict_slash=True),
  RedirectRoute(
    '/add_credits/',
    handlers.AddCreditsHandler,
    name='add-credits',
    strict_slash=True),
  RedirectRoute(
    '/_ah/health_check',
    handlers.HealthCheckHandler,
    name='health-check',
    strict_slash=True),
]


def get_routes():
  return _routes


def add_routes(app):
  if app.debug:
    secure_scheme = 'http'
  for r in _routes:
    app.router.add(r)
