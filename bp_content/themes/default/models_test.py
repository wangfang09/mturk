import unittest
from bp_content.themes.default import models


class TestUserModelBehavior(unittest.TestCase):

  def testGettingNicknameWhenNoName(self):
    user = models.User(email="xhc@gmail.com", password="xhc")
    self.assertEquals("xhc@gmail.com", user.GetNickname())
