PUNCTUATIONS = set(r"!.,:\"')")


def JoinWordsWithSpace(words):
  words = [w for w in words if w != '']
  if len(words) <= 1:
    return ''.join(words)
  spans = [words[0]]
  for i in range(len(words) - 1):
    prev = words[i]
    cur = words[i + 1]
    has_space = True

    if cur[0] in PUNCTUATIONS:
      has_space = False
    if cur == "''":
      has_space = False
    if prev == '``':
      has_space = False
    if prev == '(':
      has_space = False
    if cur == "n't":
      has_space = False

    if has_space:
      spans.append(' ')
    spans.append(cur)

  return ''.join(spans)
