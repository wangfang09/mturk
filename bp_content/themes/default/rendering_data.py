import sequences
import string_utils
import logging

from protorpc import messages


class AnnotationSpan(messages.Message):
  original_words = messages.StringField(1)
  revise_into = messages.StringField(2)


class RevisionResultSentence(messages.Message):
  annotation_spans = messages.MessageField(AnnotationSpan, 1, repeated=True)
  annotation_request_pending = messages.BooleanField(2)
  finished_annotation = messages.BooleanField(3)


class RevisionResultParagraph(messages.Message):
  sentences = messages.MessageField(RevisionResultSentence, 1, repeated=True)


class RevisionResultResponse(messages.Message):
  paragraphs = messages.MessageField(RevisionResultParagraph, 1, repeated=True)
  title = messages.StringField(2)
  price = messages.FloatField(3)
  paypal_url = messages.StringField(4)
  url = messages.StringField(5)
  archive_url = messages.StringField(6)
  payment_pending = messages.BooleanField(7)


class SentenceDisplayData(object):
  """Data interface for rendering.
  """

  def __init__(self, sentence, annotation):
    self.sentence = sentence
    self.annotation = annotation

  def GetCorrectionSpans(self):
    seq = sequences.AnnotatableSequence(self.sentence.words)
    sent_spans = []
    if self.annotation:
      for annot in self.annotation.result:
        (s, e, t) = (annot.start, annot.end, annot.revise_into)
        try:
          seq.Annotate(s, e + 1, t)
          
        except Exception as ex:
          logging.error("Error processing annotation %r" % ((s, e, t),))
    for words, revised_to in seq.GetSegments():
      sent_spans.append((string_utils.JoinWordsWithSpace(words), revised_to))
    return sent_spans

  def AnnotationRequestPending(self):
    """Whether the user wanted annotations for this sentence.
    """
    if self.sentence.out_for_annotations:
      if self.annotation is not None:
        return (self.annotation.phase != 'DONE')
      else:  # request not generated yet
        return True
    return False

  def FinishedAnnotation(self):
    if self.annotation is None:
      return False
    return self.annotation.phase == 'DONE'

  def OutForAnnotations(self):
    return self.sentence.out_for_annotations


class ArticleDisplayData(object):

  def __init__(self, title, url, price, paypal_url,
               archive_url, payment_pending):
    self.paragraphs = []
    self.title = title
    self.price = price
    self.paypal_url = paypal_url
    self.url = url
    self.archive_url = archive_url
    self.payment_pending = payment_pending

  def AddParagraph(self, par_data):
    self.paragraphs.append(par_data)

  def GetParagraphs(self):
    return self.paragraphs

  def IterSentences(self):
    for par in self.GetParagraphs():
      for sent in par:
        yield sent

  def AllFinished(self):
    return (self.GetNFinished() == self.GetNRequested())

  def GetNFinished(self):
    return sum(sent.FinishedAnnotation() for sent in self.IterSentences())

  def GetNRequested(self):
    return sum(sent.OutForAnnotations() for sent in self.IterSentences())

  def GetFinishedPercentage(self):
    return 100.0 * self.GetNFinished() / self.GetNRequested()

  def GetPaypalURL(self):
    return self.paypal_url

  def GetArchiveUrl(self):
    return self.archive_url

  def GetTitle(self):
    return self.title

  def PaymentPending(self):
    return self.payment_pending

  def GetURL(self):
    return self.url

  def GetPrice(self):
    return self.price

  def PutIntoMessage(self):
    paragraphs = []
    for par in self.paragraphs:
      sentences = []
      for sent in par:
        annotation_spans = []
        for orig, rev in sent.GetCorrectionSpans():
          annotation_spans.append(
            AnnotationSpan(
              original_words=orig,
              revise_into=rev))

        new_sent = RevisionResultSentence(
          annotation_spans=annotation_spans,
          annotation_request_pending=sent.AnnotationRequestPending(),
          finished_annotation=sent.FinishedAnnotation())
        sentences.append(new_sent)
      paragraphs.append(RevisionResultParagraph(sentences=sentences))

    message = RevisionResultResponse(
      title=self.title,
      price=self.price,
      paypal_url=self.paypal_url,
      url=self.url,
      archive_url=self.url,
      payment_pending=self.payment_pending,
      paragraphs=paragraphs
    )

    return message

  def PutIntoDict(self):
    paragraphs = []
    for par in self.paragraphs:
      sentences = []
      for sent in par:
        annotation_spans = []
        for orig, rev in sent.GetCorrectionSpans():
          annotation_spans.append({"original_words": orig, "revise_into": rev})

        new_sent = {
          "annotation_spans": annotation_spans,
          "annotation_request_pending": sent.AnnotationRequestPending(),
          "finished_annotation": sent.FinishedAnnotation()}
        sentences.append(new_sent)
      paragraphs.append({"sentences": sentences})

    result = {
      "title": self.title,
      "price": self.price,
      "paypal_url": self.paypal_url,
      "url": self.url,
      "archive_url": self.url,
      "payment_pending": self.payment_pending,
      "paragraphs": paragraphs
    }

    return result
