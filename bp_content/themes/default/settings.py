import os
import logging
import json


_SETTING_JSON_FILE_LOC = os.path.join(
  os.path.dirname(os.path.realpath(__file__)),
  "config/aws_settings.json")

admin_email = 'xue.huichao@gmail.com'

if 'SERVER_SOFTWARE' not in os.environ or os.environ[
    'SERVER_SOFTWARE'].startswith('Development'):
  CONFIG_PROFILE = 'dev'
elif 'Google App Engine' in os.environ['SERVER_SOFTWARE']:
  CONFIG_PROFILE = 'appengine'
elif 'AppScale' in os.environ['SERVER_SOFTWARE']:
  CONFIG_PROFILE = 'prod'
logging.info('Using %s configuration profile', CONFIG_PROFILE)

with open(_SETTING_JSON_FILE_LOC) as json_setting_file:
  JSON_SETTINGS = json.loads(json_setting_file.read())[CONFIG_PROFILE]

SNIPPET_LENG = 20
SQS_NUM_MESSAGES = 10
SQS_REGION_NAME = JSON_SETTINGS['aws_region']

MTURK_ACCESS_ID = JSON_SETTINGS['aws_access_key_id']
MTURK_SECRET_KEY = JSON_SETTINGS['aws_secret_access_key']

MTURK_SQS_NAME = JSON_SETTINGS['sqs_name']
MTURK_SQS_URL = JSON_SETTINGS['sqs_path']
MTURK_HOST = JSON_SETTINGS['mturk_host']
MTURK_LAYOUT_ID = JSON_SETTINGS['layout_id']
MTURK_HITTYPE_MAP = dict((int(k), v) for k, v in JSON_SETTINGS[
                         "cent_hittype_map"].iteritems())

API_ROOT = "/_ah/api"
NO_TURK_QUALIFICATIONS = JSON_SETTINGS['no_turk_qualifications']

if CONFIG_PROFILE == 'dev':
  PAYPAL_URL = 'https://www.sandbox.paypal.com/cgi-bin/webscr'
  PAYPAL_BUSINESS_ID = '6DWSVUL2QTQ9C'

elif CONFIG_PROFILE in set(['prod', 'appengine']):
  PAYPAL_URL = 'https://www.paypal.com/cgi-bin/webscr'
  PAYPAL_BUSINESS_ID = 'EB2HB74HJRANQ'

TESTDRIVE_MAX_TITLE_LENGTH = 400
TESTDRIVE_MAX_SENT = 33
TESTDRIVE_MAX_LENGTH = TESTDRIVE_MAX_SENT * 40 * 10
TESTDRIVE_PRICE_CHOICE = "PREMIUM"

INITIAL_USER_CREDITS = 200

# In dollar values
TURKER_COST_DOLLARS = {
  'BASIC': 0.01,
  'PRIORITY': 0.02,
  'PREMIUM': 0.03,
}

# In credits
USER_CHARGE_CREDITS = {
  'BASIC': 3,
  'PRIORITY': 4,
  'PREMIUM': 5,
}

logging.info("Settings:")
for var_name, var_val in globals().items():
  logging.info("%s=%r", var_name, var_val)
