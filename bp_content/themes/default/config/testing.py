import os

config = {

  # This config file will be detected in localhost environment and values
  # defined here will overwrite those in config.py
  'environment': "testing",

  # webapp2 sessions
  'webapp2_extras.sessions': {'secret_key': 'bighead_webapp2_secret_key'},

  # webapp2 authentication
  'webapp2_extras.auth': {'user_model': 'bp_content.themes.default.models.User',
                          'cookie_name': 'session_name'},

  # jinja2 templates
  'webapp2_extras.jinja2': {'template_path': ['bp_admin/templates', 'bp_content/themes/%s/templates' % os.environ['theme']],
                            'environment_args': {'extensions': ['jinja2.ext.i18n']}},

  # application name
  'app_name': "Crowd Grammar",

  # Locale code = <language>_<territory> (ie 'en_US')
  # to pick locale codes see http://cldr.unicode.org/index/cldr-spec/picking-the-right-language-code
  # also see http://www.sil.org/iso639-3/codes.asp
  # Language codes defined under iso 639-1 http://en.wikipedia.org/wiki/List_of_ISO_639-1_codes
  # Territory codes defined under iso 3166-1 alpha-2 http://en.wikipedia.org/wiki/ISO_3166-1
  # disable i18n if locales array is empty or None
  'locales': ['en_US', 'zh_CN'],
  # Commented out: 'es_ES', 'it_IT', 'id_ID', 'fr_FR', 'de_DE', 'ru_RU',
  # 'pt_BR', 'cs_CZ','vi_VN', 'nl_NL', 'ko_KR'

  # contact page email settings
  'contact_sender': "coding.chitu@gmail.com",
  'contact_recipient': "coding.chitu@gmail.com",

  # Password AES Encryption Parameters
  # aes_key must be only 16 (*AES-128*), 24 (*AES-192*), or 32 (*AES-256*)
  # bytes (characters) long.
  'aes_key': "01234567890123456789012345678901",
  'salt': "01234567890123456789012345678901",

  # get your own recaptcha keys by registering at
  # http://www.google.com/recaptcha/
  'captcha_public_key': "6LcPtBMTAAAAACcRcYWfo3nleXyiafWdTEpX07ES",
  'captcha_private_key': "6LcPtBMTAAAAAPFp2wAqK8h8HaDJU9jEujUZhKUg",

  # Use a complete Google Analytics code, no just the Tracking ID
  # In config/boilerplate.py there is an example to fill out this value
  'google_analytics_code': """""",

  # fellas' list
  'developers': (
    ('Chitu', 'coding.chitu@gmail.com'),
  ),
  'enable_federated_login': False,

  # NLP server configuration
  'nlp_server': 'getop.xuehuichao.com:12085',
}
