from datetime import timedelta
from boto import sqs
from boto.mturk.connection import MTurkConnection
from boto.mturk.qualification import LocaleRequirement
from boto.mturk.qualification import NumberHitsApprovedRequirement
from boto.mturk.qualification import PercentAssignmentsApprovedRequirement
from boto.mturk.qualification import Qualifications
from boto.mturk.layoutparam import LayoutParameter
from boto.mturk.layoutparam import LayoutParameters
from boto.sqs import connection as SQSConnection
import json

from google.appengine.ext import ndb
import sys
import logging

from bp_content.themes.default import models
from bp_content.themes.default import settings


class TurkConfig(object):

  def __init__(self):
    if settings.NO_TURK_QUALIFICATIONS:
      self.quals = None
    else:
      self.quals = Qualifications()
      self.quals.add(
        LocaleRequirement("EqualTo", "US"))
      self.quals.add(
        NumberHitsApprovedRequirement("GreaterThanOrEqualTo", 10))
      self.quals.add(
        PercentAssignmentsApprovedRequirement("GreaterThanOrEqualTo", 95))
      
    self.MTURK_ACCESS_ID = settings.MTURK_ACCESS_ID
    self.MTURK_SECRET_KEY = settings.MTURK_SECRET_KEY
    self.MTURK_HOST = settings.MTURK_HOST
    self.MTURK_LAYOUT_ID = settings.MTURK_LAYOUT_ID
    self.MTURK_HIT_TYPE_ID_MAP = settings.MTURK_HITTYPE_MAP
    self.MTURK_DEFAULT_LIFETIME = timedelta(days=1)
    self.MTURK_SQS_URL = settings.MTURK_SQS_URL
    self.SQS_NAME = settings.MTURK_SQS_NAME
    
    
    self.mtc = MTurkConnection(
      aws_access_key_id=self.MTURK_ACCESS_ID,
      aws_secret_access_key=self.MTURK_SECRET_KEY,
      host=self.MTURK_HOST)
    
    for hit_type_id in self.MTURK_HIT_TYPE_ID_MAP.itervalues():
      self.mtc.set_sqs_notification(
        hit_type_id, self.MTURK_SQS_URL, 'HITReviewable')

    sqs_region = filter(
      lambda x: x.name == settings.SQS_REGION_NAME,
      sqs.regions())[0]
    
    logging.info("%r",sqs_region)
    self.sqs_connection = SQSConnection.SQSConnection(
      aws_access_key_id=self.MTURK_ACCESS_ID,
      aws_secret_access_key=self.MTURK_SECRET_KEY,
      region=sqs_region,
    )
    logging.info("%s","sqs_connection created")
    self.turk_queue = self.sqs_connection.get_queue(self.SQS_NAME)
    logging.info("%r",self.turk_queue)
  the_config = None

  @staticmethod
  def GetTheConfig():
    if TurkConfig.the_config is None:
      TurkConfig.the_config = TurkConfig()
    return TurkConfig.the_config

  def CreateHit(self, words, surface, previous_sentence, next_sentence, cents):
    params = LayoutParameters([LayoutParameter('sent', ' '.join(words)),
                               LayoutParameter('surface', surface),
                               LayoutParameter('previous_sentence', previous_sentence),
                               LayoutParameter('next_sentence', next_sentence)])
    
    hit = self.mtc.create_hit(
      hit_type=self.MTURK_HIT_TYPE_ID_MAP[cents],
      hit_layout=self.MTURK_LAYOUT_ID,
      layout_params=params,
      lifetime=self.MTURK_DEFAULT_LIFETIME,
      max_assignments=2,
    )
    """logging.info("%s","hit created")"""
    return hit

  def CreateVoteHit(self, orig_sur, words, revision_1, revision_2, cents):
    params = LayoutParameters([LayoutParameter('original_surface',orig_sur),
                               LayoutParameter('original_sent',' '.join(words)),
                               LayoutParameter('revision_1',revision_1),
                               LayoutParameter('revision_2',revision_2)])
    hit = self.mtc.create_hit(hit_type=self.MTURK_HIT_TYPE_ID_MAP[cents], 
                              hit_layout='3X6YVLARM6FHNFU4PEEV3T52UEYO75', 
                              layout_params=params, 
                              lifetime=self.MTURK_DEFAULT_LIFETIME, 
                              max_assignments=1)
    """hit_layout='3247MKAWG5D0HKHTJXYD197GBSJT4W', for sandbox"""
    """hit_layout='3X6YVLARM6FHNFU4PEEV3T52UEYO75', for turk"""
    logging.info("%s","vote_hit created")
    return hit

  def ExpireHit(self, hit_id):
    self.mtc.expire_hit(hit_id)

  def AccountBalance(self):
    return self.mtc.get_account_balance()[0].amount

  def GetHitAnnotation(self, annot_req, hit_id):
    if self.mtc.get_assignments(hit_id).__len__() > 1:   
        assignment_1 = self.mtc.get_assignments(hit_id)[0]
        assignment_2 = self.mtc.get_assignments(hit_id)[1]
        annotation_1 = json.loads(assignment_1.answers[0][0].fields[0])
        annotation_2 = json.loads(assignment_2.answers[0][0].fields[0])
        suggestions_1 = [(sug['s'], sug['e'], sug['revision']) for sug in annotation_1]
        suggestions_2 = [(sug['s'], sug['e'], sug['revision']) for sug in annotation_2]
        
        for s, e, t in suggestions_1:
            revision_1 = t
        for s, e, t in suggestions_2:
            revision_2 = t
        original_sentence = annot_req.GetSentence().surface
        words = annot_req.GetSentence().words
        vote_hit = TurkConfig.GetTheConfig().CreateVoteHit(
            original_sentence, words, revision_1, revision_2, annot_req.GetPriceInCents())[0]
        annot_req.hit_id = vote_hit.HITId
        annot_req.put()
        return None
    
    assignments = self.mtc.get_assignments(hit_id)[0]
    return assignments.answers[0][0].fields[0]

  def GetAvailableHitsInQueue(self):
    messages = self.turk_queue.get_messages(settings.SQS_NUM_MESSAGES)
    for msg in messages:
      msg_json = json.loads(msg.get_body())
      self.sqs_connection.delete_message(self.turk_queue, msg)
      yield msg_json['Events'][0]['HITId']


def PostSentenceRequest(sent_annot_req):
  logging.info("%r",sent_annot_req.GetSentence)
  words = sent_annot_req.GetSentence().words
  surface = sent_annot_req.GetSentence().surface
  previous_sentence = sent_annot_req.GetSentence().previous_sentence
  next_sentence = sent_annot_req.GetSentence().next_sentence
  try:
    hit = TurkConfig.GetTheConfig().CreateHit(
      words, surface, previous_sentence, next_sentence, sent_annot_req.GetPriceInCents())[0]
    
    sent_annot_req.hit_id = hit.HITId
    sent_annot_req.phase = 'SUBMITTED'
  except Exception as e:
    sent_annot_req.phase = 'SUBMISSION_ERROR'
    logging.error(
      "Submission error on %s. Exception %s",
      sent_annot_req.key.urlsafe(),
      str(e))
  sent_annot_req.put()


def PostAnnotation(annot_req, hit_id):
  annot_req.result = []

  if(TurkConfig.GetTheConfig().GetHitAnnotation(annot_req, hit_id)):
    annotations = json.loads(TurkConfig.GetTheConfig().GetHitAnnotation(annot_req, hit_id))
    suggestions = [(sug['s'], sug['e'], sug['revision']) for sug in annotations]
    logging.info("%r",suggestions[0])
    annot_req.SetSuggestions(suggestions)


def UpdateBasedonSQS():
  for available_hit in TurkConfig.GetTheConfig().GetAvailableHitsInQueue():
    results = models.SentenceAnnotationRequest.query(
      models.SentenceAnnotationRequest.hit_id == available_hit).fetch(2)
    if len(results) == 0:
      logging.error(
        "Unable to find matching annotation request: %s",
        available_hit)
      continue

    if len(results) >= 2:
      logging.error(
        "Found two annotation requests matching HITId: %s",
        available_hit)
    
    annot_req = results[0]
    PostAnnotation(annot_req, available_hit)
