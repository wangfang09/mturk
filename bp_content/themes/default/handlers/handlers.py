# -*- coding: utf-8 -*-

"""
    A real simple app for using webapp2 with auth and session.

    It just covers the basics. Creating a user, login, logout
    and a decorator for protecting certain handlers.

    Routes are setup in routes.py and added in main.py
"""
# standard library imports
import sys
import json
import urllib
import re
import logging
import os
import jinja2
import datetime
import itertools

# related third party imports
import webapp2
import wtforms
from google.appengine.ext import ndb
from google.appengine.ext.webapp import template
from google.appengine.api import taskqueue
from google.appengine.api import urlfetch
from webapp2_extras.auth import InvalidAuthIdError, InvalidPasswordError
from webapp2_extras.i18n import gettext as _
from bp_includes.external import httpagentparser

# local application/library specific imports
import bp_includes.lib.i18n as i18n
from bp_includes.lib.basehandler import BaseHandler
from bp_includes.lib import decorators
from bp_includes.lib import captcha, utils
import bp_includes.models as models_boilerplate
from webapp2_extras.i18n import lazy_gettext

from bp_content.themes.default import turk
from bp_content.themes.default import settings
from bp_content.themes.default import models
from bp_content.themes.default import pricing
from bp_content.themes.default.handlers import forms as forms
from bp_includes import forms as bp_forms
from bp_includes.external_wrap.nltk_packages import sent_tokenize


class HealthCheckHandler(BaseHandler):

  def get(self):
    self.response.write("ok")


class ContactHandler(BaseHandler):
  """
  Handler for Contact Form
  """

  def get(self):
    """ Returns a simple HTML for contact form """

    if self.user:
      user_info = self.user_model.get_by_id(long(self.user_id))
      if user_info.name or user_info.last_name:
        self.form.name.data = user_info.name + " " + user_info.last_name
      if user_info.email:
        self.form.email.data = user_info.email
    params = {
      "exception": self.request.get('exception')
    }

    return self.render_template('contact.html', **params)

  def post(self):
    """ validate contact form """
    if not self.form.validate():
      return self.get()

    remote_ip = self.request.remote_addr
    city = i18n.get_city_code(self.request)
    region = i18n.get_region_code(self.request)
    country = i18n.get_country_code(self.request)
    coordinates = i18n.get_city_lat_long(self.request)
    user_agent = self.request.user_agent
    exception = self.request.POST.get('exception')
    name = self.form.name.data.strip()
    email = self.form.email.data.lower()
    message = self.form.message.data.strip()
    template_val = {}

    challenge = self.request.POST.get('recaptcha_challenge_field')
    response = self.request.POST.get('recaptcha_response_field')
    cResponse = captcha.submit(
      challenge,
      response,
      self.app.config.get('captcha_private_key'),
      remote_ip)

    if re.search(
        r"(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})", message) and not cResponse.is_valid:
      chtml = captcha.displayhtml(
        public_key=self.app.config.get('captcha_public_key'),
        use_ssl=(self.request.scheme == 'https'),
        error=None)
      if self.app.config.get('captcha_public_key') == "PUT_YOUR_RECAPCHA_PUBLIC_KEY_HERE" or \
          self.app.config.get('captcha_private_key') == "PUT_YOUR_RECAPCHA_PUBLIC_KEY_HERE":
        chtml = '<div class="alert alert-danger"><strong>Error</strong>: You have to ' \
                '<a href="http://www.google.com/recaptcha/whyrecaptcha" target="_blank">sign up ' \
                'for API keys</a> in order to use reCAPTCHA.</div>' \
                '<input type="hidden" name="recaptcha_challenge_field" value="manual_challenge" />' \
                '<input type="hidden" name="recaptcha_response_field" value="manual_challenge" />'
      template_val = {
        "captchahtml": chtml,
        "exception": exception,
        "message": message,
        "name": name,

      }
      if not cResponse.is_valid and response is None:
        _message = _(
          "Please insert the Captcha in order to finish the process of sending the message")
        self.add_message(_message, 'warning')
      elif not cResponse.is_valid:
        _message = _('Wrong image verification code. Please try again.')
        self.add_message(_message, 'danger')

      return self.render_template('contact.html', **template_val)
    else:
      try:
        # parsing user_agent and getting which os key to use
        # windows uses 'os' while other os use 'flavor'
        ua = httpagentparser.detect(user_agent)
        _os = 'flavor' in ua and 'flavor' or 'os'

        operating_system = str(ua[_os]['name']) if "name" in ua[_os] else "-"
        if 'version' in ua[_os]:
          operating_system += ' ' + str(ua[_os]['version'])
        if 'dist' in ua:
          operating_system += ' ' + str(ua['dist'])

        browser = str(ua['browser']['name']) if 'browser' in ua else "-"
        browser_version = str(
          ua['browser']['version']) if 'browser' in ua else "-"

        template_val = {
          "name": name,
          "email": email,
          "ip": remote_ip,
          "city": city,
          "region": region,
          "country": country,
          "coordinates": coordinates,

          "browser": browser,
          "browser_version": browser_version,
          "operating_system": operating_system,
          "message": message
        }
      except Exception as e:
        logging.error("error getting user agent info: %s" % e)

      try:
        subject = _("Contact") + " " + self.app.config.get('app_name')
        # exceptions for error pages that redirect to contact
        if exception != "":
          subject = "{} (Exception error: {})".format(subject, exception)

        body_path = "emails/contact.txt"
        body = self.jinja2.render_template(body_path, **template_val)

        email_url = self.uri_for('taskqueue-send-email')
        taskqueue.add(url=email_url, params={
          'to': self.app.config.get('contact_recipient'),
          'subject': subject,
          'body': body,
          'sender': self.app.config.get('contact_sender'),
        })

        message = _('Your message was sent successfully.')
        self.add_message(message, 'success')
        return self.redirect_to('contact')

      except (AttributeError, KeyError) as e:
        logging.error('Error sending contact form: %s' % e)
        message = _('Error sending the message. Please try again later.')
        self.add_message(message, 'danger')
        return self.redirect_to('contact')

  @webapp2.cached_property
  def form(self):
    return forms.ContactForm(self)


class SecureRequestHandler(BaseHandler):
  """
  Only accessible to users that are logged in
  """

  @decorators.user_required
  def get(self, **kwargs):
    user_session = self.user
    user_session_object = self.auth.store.get_session(self.request)

    user_info = self.user_model.get_by_id(long(self.user_id))
    user_info_object = self.auth.store.user_model.get_by_auth_token(
      user_session['user_id'], user_session['token'])

    try:
      params = {
        "user_session": user_session,
        "user_session_object": user_session_object,
        "user_info": user_info,
        "user_info_object": user_info_object,
        "userinfo_logout-url": self.auth_config['logout_url'],
      }
      return self.render_template('secure_zone.html', **params)
    except (AttributeError, KeyError) as e:
      return "Secure zone error:" + " %s." % e


class DeleteAccountHandler(BaseHandler):

  @decorators.user_required
  def get(self, **kwargs):
    chtml = captcha.displayhtml(
      public_key=self.app.config.get('captcha_public_key'),
      use_ssl=(self.request.scheme == 'https'),
      error=None)
    if self.app.config.get('captcha_public_key') == "PUT_YOUR_RECAPCHA_PUBLIC_KEY_HERE" or \
        self.app.config.get('captcha_private_key') == "PUT_YOUR_RECAPCHA_PUBLIC_KEY_HERE":
      chtml = '<div class="alert alert-danger"><strong>Error</strong>: You have to ' \
              '<a href="http://www.google.com/recaptcha/whyrecaptcha" target="_blank">sign up ' \
              'for API keys</a> in order to use reCAPTCHA.</div>' \
              '<input type="hidden" name="recaptcha_challenge_field" value="manual_challenge" />' \
              '<input type="hidden" name="recaptcha_response_field" value="manual_challenge" />'
    params = {
      'captchahtml': chtml,
    }
    return self.render_template('delete_account.html', **params)

  def post(self, **kwargs):
    challenge = self.request.POST.get('recaptcha_challenge_field')
    response = self.request.POST.get('recaptcha_response_field')
    remote_ip = self.request.remote_addr

    cResponse = captcha.submit(
      challenge,
      response,
      self.app.config.get('captcha_private_key'),
      remote_ip)

    if cResponse.is_valid:
      # captcha was valid... carry on..nothing to see here
      pass
    else:
      _message = _('Wrong image verification code. Please try again.')
      self.add_message(_message, 'danger')
      return self.redirect_to('delete-account')

    if not self.form.validate() and False:
      return self.get()
    password = self.form.password.data.strip()

    try:

      user_info = self.user_model.get_by_id(long(self.user_id))
      auth_id = "own:%s" % user_info.username
      password = utils.hashing(password, self.app.config.get('salt'))

      try:
        # authenticate user by its password
        user = self.user_model.get_by_auth_password(auth_id, password)
        if user:
          # Delete Social Login
          for social in models_boilerplate.SocialUser.get_by_user(
              user_info.key):
            social.key.delete()

          user_info.key.delete()

          ndb.Key("Unique", "User.username:%s" % user.username).delete_async()
          ndb.Key(
            "Unique",
            "User.auth_id:own:%s" %
            user.username).delete_async()
          ndb.Key("Unique", "User.email:%s" % user.email).delete_async()

          # TODO: Delete UserToken objects

          self.auth.unset_session()

          # display successful message
          msg = _("The account has been successfully deleted.")
          self.add_message(msg, 'success')
          return self.redirect_to('home')

      except (InvalidAuthIdError, InvalidPasswordError) as e:
        # Returns error message to self.response.write in
        # the BaseHandler.dispatcher
        message = _(
          "Incorrect password! Please enter your current password to change your account settings.")
        self.add_message(message, 'danger')
      return self.redirect_to('delete-account')

    except (AttributeError, TypeError) as e:
      login_error_message = _('Your session has expired.')
      self.add_message(login_error_message, 'danger')
      self.redirect_to('login')

  @webapp2.cached_property
  def form(self):
    return forms.DeleteAccountForm(self)


class BasicRequestHandler(BaseHandler):

  def Render(self, template_file, render_dict={}):
    user = models.User.GetCurrentUser(self)

    render_dict['nickname'] = user.GetNickname() if user is not None else None
    render_dict['credits'] = user.GetCredits() if user is not None else None
    render_dict['login_url'] = '/login'
    render_dict['base_url'] = self.request.application_url
    render_dict['API_ROOT'] = settings.API_ROOT

    self.render_template(template_file, **render_dict)


class MainPageHandler(BasicRequestHandler):

  def get(self):
    user = models.User.GetCurrentUser(self)
    if user is not None:
      self.redirect('/dashboard')

    else:
      self.redirect('/intro')


class IntroPageHandler(BasicRequestHandler):

  def get(self):
    self.Render('intro.html')


class TestDriveForm(bp_forms.EmailMixin, bp_forms.PasswordConfirmMixin):

  title = wtforms.fields.TextField(lazy_gettext('Title'), [
    wtforms.validators.Required(),
    wtforms.validators.Length(max=settings.TESTDRIVE_MAX_TITLE_LENGTH),
  ])
  content = wtforms.fields.TextAreaField(lazy_gettext('Content'), [
    wtforms.validators.Required(),
    wtforms.validators.Length(max=settings.TESTDRIVE_MAX_LENGTH),
  ])

  def validate_content(form, field):
    # Check that its number of sentences is under limit.
    if len(sent_tokenize(field.data)) > settings.TESTDRIVE_MAX_SENT:
      raise wtforms.ValidationError(
        _('Number of sentences must be under ') + str(settings.TESTDRIVE_MAX_SENT))


class TestDriveHandler(BasicRequestHandler):

  @webapp2.cached_property
  def form(self):
    f = TestDriveForm(self)
    return f

  def get(self):
    self.Render('testdrive.html', {'form': self.form,
                                   'url': self.request.url,
                                   'max_sent': settings.TESTDRIVE_MAX_SENT,
                                   })

  def post(self):
    if not self.form.validate():
      return self.get()

    email = self.form.email.data.lower()
    username = email
    password = self.form.password.data.strip()
    title = self.form.title.data
    content = self.form.content.data

    # Password to SHA512
    password = utils.hashing(password, self.app.config.get('salt'))

    # Passing password_raw=password so password will be hashed
    # Returns a tuple, where first value is BOOL, second is the actual user.
    # If True ok, If False no new user is created
    unique_properties = ['username', 'email']
    auth_id = "own:%s" % username
    user = self.auth.store.user_model.create_user(
      auth_id, unique_properties, password_raw=password,
      username=username, email=email, ip=self.request.remote_addr,
    )

    if not user[0]:  # user is a tuple
      if "username" in str(user[1]):
        message = _(
          'Sorry, The username <strong>{}</strong> is already registered.').format(username)
      elif "email" in str(user[1]):
        message = _(
          'Sorry, The email <strong>{}</strong> is already registered.').format(email)
      else:
        message = _('Sorry, The user is already registered.')
      self.add_message(message, 'danger')
      return self.redirect_to('testdrive')
    else:
      # User registered successfully
      user[1].PreprocessThenAddAnnotationRequest(
        content, title, settings.TESTDRIVE_PRICE_CHOICE)

      subject = _("%s Account Verification" % self.app.config.get('app_name'))
      confirmation_url = self.uri_for("account-activation",
                                      user_id=user[1].get_id(),
                                      token=self.user_model.create_auth_token(
                                        user[1].get_id()),
                                      _full=True)

      # load email's template
      template_val = {
        "app_name": self.app.config.get('app_name'),
        "username": username,
        "confirmation_url": confirmation_url,
        "support_url": self.uri_for("contact", _full=True),
        "title": title,
        "content": content
      }
      body_path = "emails/account_activation_with_testdrive.txt"
      body = self.jinja2.render_template(body_path, **template_val)

      email_url = self.uri_for('taskqueue-send-email')
      taskqueue.add(url=email_url, params={
        'to': str(email),
        'subject': subject,
        'body': body,
      })

      message = _('You were successfully registered. '
                  'Please check your email to activate your account.'
                  'We will start reviewing the article soon after account activation.')
      self.add_message(message, 'success')
      return self.redirect_to('home')


class DashboardHandler(BasicRequestHandler):

  @decorators.user_required
  def get(self):
    user = models.User.GetCurrentUser(self)
    self.Render('dashboard.html',
                {'annotation_requests': [i for i in user.GetVisibleAnnotationRequests()]})


class AddRequestHandler(BasicRequestHandler):

  @decorators.user_required
  def get(self):
    user = models.User.GetCurrentUser(self)
    self.Render('add_request.html', {"user_urlsafe": user.key.urlsafe()})

  @decorators.user_required
  def post(self):
    user = models.User.GetCurrentUser(self)
    sent_ids = map(int, json.loads(self.request.get('activate_list')))
    price_choice = self.request.get('price_choice')

    annotation_request = user.AddAnnotationRequest(
      self.request.get('content'),
      self.request.get('title'),
      json.loads(self.request.get('paragraphs')),
      sent_ids,
      price_choice,
    )
    logging.info("User %s is submitting %d sentences with price choice: %s. "
                 "Creating annotation request ID: %s",
                 user.GetNickname(), len(sent_ids), price_choice, annotation_request.key)

    the_key = annotation_request.put()
    if self.request.get('payment_type') == 'credits':
      total_credits = settings.USER_CHARGE_CREDITS[
        price_choice] * len(sent_ids)
      annotation_request.amount = total_credits / 100.0
      annotation_request.put()
      logging.info(
        "User %s (%d credits remaining) trying to pay for %s, which costs %d credits.",
        user.GetNickname(), user.credits, annotation_request.key, total_credits)
      if user.credits < total_credits:
        self.add_message(_("Insufficient Credits."), 'danger')
        self.redirect_to('dashboard')
        return
      user.credits -= total_credits
      user.put()
      PaypalIPNHandler.OnPaymentSuccess(annotation_request)
      self.add_message(
        'Article <a href="{}">{}</a> is being submitted for review.'.format(
          annotation_request.GetURL(), annotation_request.GetTitle()))
      self.redirect_to('dashboard')
    elif self.request.get('payment_type') == 'paypal':
      unit_price = settings.USER_CHARGE_CREDITS[price_choice] / 100.0
      annotation_request.amount = max(
        1.0, len(sent_ids) * (0.02 + unit_price) + 0.3)
      annotation_request.put()
      annotation_request.RenderPaypalRedirect(self)


class PaypalHandler(BasicRequestHandler):

  def get(self, annotation_request_urlsafe):
    annotation_request = ndb.Key(urlsafe=annotation_request_urlsafe).get()
    annotation_request.RenderPaypalRedirect(self)


class ShowArticleHandler(BasicRequestHandler):

  @decorators.user_required
  def get(self, article_key_url):
    try:
      the_article = ndb.Key(urlsafe=article_key_url).get()
    except Exception as e:
      logging.error('Unable to get URL at: %s', article_key_url)
      self.error(404)
      self.response.write('Unable to get URL at: %s' % article_key_url)
      return

    author = the_article.GetAuthor()
    cur_user = models.User.GetCurrentUser(self)
    if author != cur_user:
      logging.error('Access denied for URL at: %s', article_key_url)
      self.error(401)
      self.response.write('Access denied for URL at: %s' % article_key_url)
      return

    self.Render('article.html', {'article': the_article.GetRenderingData(),
                                 'article_urlsafe': the_article.key.urlsafe()})


class ArchiveHandler(BasicRequestHandler):

  @decorators.user_required
  def get(self, article_key_url):
    try:
      the_article = ndb.Key(urlsafe=article_key_url).get()
    except Exception as e:
      logging.error('Unable to get URL at: %s', article_key_url)
      self.error(404)
      self.response.write('Unable to get URL at: %s' % article_key_url)
      return

    author = the_article.GetAuthor()
    cur_user = models.User.GetCurrentUser(self)
    if author != cur_user:
      logging.error('Access denied for URL at: %s', article_key_url)
      self.error(401)
      self.response.write('Access denied for URL at: %s' % article_key_url)
      return

    the_article.Archive()

    self.redirect('/dashboard')


class ShowAllSentRequestsHandler(BasicRequestHandler):

  @decorators.admin_required
  def get(self):
    self.Render(
      'turk_dash.html', {
        'requests': models.SentenceAnnotationRequest.GetOpenRequests()})


class AnnotationHandler(BasicRequestHandler):

  def get(self, request_url_key):
    request_key = ndb.Key(urlsafe=request_url_key)
    request = request_key.get()
    sentence = request.GetSentence()

    self.Render('annotate_request.html',
                {'sentence': sentence,
                 'request_urlsafe': request_url_key})

  def post(self):
    annotations = json.loads(self.request.get('annotation'))
    logging.info("%r",annotations)
    request_urlsafe = self.request.get('request_urlsafe')
    request_key = ndb.Key(urlsafe=request_urlsafe)
    request = request_key.get()
    request.result = []

    suggestions = [(sug['s'], sug['e'], sug['revision'])
                   for sug in annotations]
    """suggestions = annotations"""
    request.SetSuggestions(suggestions)

    self.redirect('/turk')


class AddCreditsHandler(BasicRequestHandler):

  @decorators.user_required
  def get(self):
    self.Render('add_credits.html', {})

  @decorators.user_required
  def post(self):
    """Create a transaction with user, payment and credits, then redirect to paypal."""
    user = models.User.GetCurrentUser(self)
    n_credits = int(self.request.get("credits"))
    order = models.AddCreditsOrder(
      user=user.key,
      n_credits=n_credits,
      amount=pricing.OrderPriceForCredits(n_credits),
      stage='CREATED',
    )
    order.put()
    order.RenderPaypalRedirect(self)


class PaypalIPNHandler(BasicRequestHandler):

  @staticmethod
  def OnPaymentSuccess(order):
    taskqueue.add(
      url="/taskqueue_mark_order_as_paid/",
      params={"order_key": order.key.urlsafe()})

  @staticmethod
  def HandleVerifiedRequest(request):
    if request.get('payment_status') != 'Completed':
      return

    request_urlsafe = request.get('custom')
    order = ndb.Key(urlsafe=request_urlsafe).get()
    PaypalIPNHandler.OnPaymentSuccess(order)

  def post(self):
    logging.info(list(self.request.POST.iteritems()))
    post_fields = [('cmd', '_notify-validate')
                   ] + list(self.request.POST.iteritems())
    form_data = urllib.urlencode(post_fields)
    result = urlfetch.fetch(url=settings.PAYPAL_URL,
                            payload=form_data,
                            method=urlfetch.POST)

    logging.info(result.content)
    if result.content == 'INVALID':
      return

    elif result.content == 'VERIFIED':
      self.HandleVerifiedRequest(self.request)

    else:
      return


class SubmitTurkerRequestHandler(BasicRequestHandler):

  @decorators.taskqueue_method
  def post(self):
    key = self.request.get('request_key')
    turk.PostSentenceRequest(ndb.Key(urlsafe=key).get())
    logging.info("%r",ndb.Key(urlsafe=key).get())


class UpdateSQSHandler(BasicRequestHandler):

  @decorators.cron_method
  def get(self):
    turk.UpdateBasedonSQS()


class MarkAsPaidHandler(BasicRequestHandler):

  @decorators.taskqueue_method
  def post(self):
    key = self.request.get('order_key')
    order = ndb.Key(urlsafe=key).get()
    req = order.MarkAsPaid()


class ExpireSentReqHanlder(BasicRequestHandler):

  @decorators.taskqueue_method
  def post(self):
    key = self.request.get('req_urlsafe')
    sent_req = ndb.Key(urlsafe=key).get()
    if sent_req.phase == 'SUBMITTED':
      turk.TurkConfig.GetTheConfig().ExpireHit(sent_req.hit_id)
      sent_req.phase = 'PAID'
      sent_req.hit_id = None
      sent_req.put()
