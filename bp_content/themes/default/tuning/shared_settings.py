import os
import sys

SDK_PATH = '/usr/local/google_appengine'
if os.path.exists(os.path.join(SDK_PATH, 'platform/google_appengine')):
  sys.path.insert(0, os.path.join(SDK_PATH, 'platform/google_appengine'))
else:
  sys.path.insert(0, SDK_PATH)

import dev_appserver
dev_appserver.fix_sys_path()

# Loading appengine_config from the current project ensures that any
# changes to configuration there are available to all tests (e.g.
# sys.path modifications, namespaces, etc.)
try:
  import appengine_config
  (appengine_config)
except ImportError:
  print 'Note: unable to import appengine_config.'
