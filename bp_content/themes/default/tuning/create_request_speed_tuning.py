import shared_settings
from tests import utils
import unittest
import cProfile
import json


class TestImportingBigFile(utils.BasicSetupFixture):

  def testPostingALargeRequest(self):
    paragraphs = []
    words = ["word%.2d" % i for i in range(20)]
    for i in range(100):
      par = {"sentences": [{"words": words}] * 10}
      paragraphs.append(par)
    self.app.post(
      '/add_request',
      {
        'activate_list': json.dumps(range(100 * 10)),
        'sent_price': "0.01",
        'content': "\n\n".join([" ".join(words * 10)] * 100),
        "title": 'test',
        'paragraphs': json.dumps({'paragraphs': paragraphs}),
      })

if __name__ == "__main__":
  cProfile.run('unittest.main()', sort="cumtime")
#    unittest.main()
