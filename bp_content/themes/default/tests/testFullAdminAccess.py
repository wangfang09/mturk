import utils
import unittest
import os
import json
import main
from webtest import TestApp
from google.appengine.ext import testbed
from google.appengine.ext import ndb

from bp_content.themes.default import models


class TestSubmitRequestViaPaypal(utils.BasicSetupFixture):

  def setUp(self):
    utils.BasicSetupFixture.setUp(self)
    self.SubmitRequestViaPaypalAndMarkPaid(
      'This is just a test request. It should be splitted up into two '
      'sentences.', [0, 1], 'BASIC')

  def testShowArticleDashboard(self):
    self.assertIn('This is just', self.testapp.get('/dashboard').body)

  def testHaveOpenRequests(self):
    requests = models.SentenceAnnotationRequest.GetOpenRequests()
    self.assertEquals(2, len(requests))
    self.assertIn('request', requests[0].GetSentence().words)

  def testTurkAnnotateRequestWithNativeInterface(self):
    soup = self.testapp.get('/turk').html
    annotation_page_link = soup.find('a', {'class': 'req_link'})['href']

    form = self.get_form(annotation_page_link, 'form')
    form['annotation'] = json.dumps([{'s': 2,
                                      'e': 2,
                                      'revision': ''}])
    self.submit(form)

    suggestions = list(models.SentenceRevisionSuggestion.query())
    self.assertEquals(1, len(suggestions))
    self.assertEquals(
      (2, 2, ''), (suggestions[0].start, suggestions[0].end,
                   suggestions[0].revise_into))

  def testGettingPriceForEachRequest(self):
    requests = models.SentenceAnnotationRequest.GetOpenRequests()
    self.assertEquals(1, requests[0].GetPriceInCents())


class TestDisplayingSentencesAndAnnotations(utils.BasicSetupFixture):

  def setUp(self):
    utils.BasicSetupFixture.setUp(self)
    self.request = self.SubmitRequestViaPaypalAndMarkPaid(
      'This is the first sentence. And this is the second sentence.\n\n'
      ' And this is the third. And this is the fourth.', [0, 1, 3], 'BASIC')

    self.article = list(models.AnnotationRequest.query())[0]
    self.sentences = self.article.GetSentences()
    self.sent_requests = list(iter(self.request))
    self.sent_requests[0].SetSuggestions([(4, 4, '')])
    self.sent_requests[1].SetSuggestions([(0, 0, ''), (5, 5, 'one')])

  def testGettingSentenceAndAnnotations(self):
    spans = []
    for sent in self.article.GetRenderingData().IterSentences():
      spans.append(sent.GetCorrectionSpans())
    self.assertEquals(
      [[('This is the first', None),
        ('sentence', ''),
        ('.', None), ],
       [('And', ''), ('this is the second', None), ('sentence', 'one'),
        ('.', None)]], spans[:2])

  def testPendingStatus(self):
    sentences = list(self.article.GetRenderingData().IterSentences())
    self.assertFalse(sentences[0].AnnotationRequestPending())
    self.assertFalse(sentences[1].AnnotationRequestPending())
    self.assertFalse(sentences[2].AnnotationRequestPending())
    self.assertTrue(sentences[3].AnnotationRequestPending())

  def testGettingSentenceByInd(self):
    self.assertIn('first', self.request.GetSentenceByInd(0).words)
    self.assertIn('second', self.request.GetSentenceByInd(1).words)
    self.assertIn('third', self.request.GetSentenceByInd(2).words)
    self.assertIn('fourth', self.request.GetSentenceByInd(3).words)


class TestDisplayingSentencesAndAnnotationsBeforePay(utils.BasicSetupFixture):

  def setUp(self):
    utils.BasicSetupFixture.setUp(self)
    self.request = self.SubmitRequestViaPaypal(
      'This is the first sentence. And this is the second sentence.\n\n'
      ' And this is the third. And this is the fourth.', [0, 1, 3])
    self.sentences = list(self.request.GetRenderingData().IterSentences())

  def testPendingStatus(self):
    self.assertTrue(self.sentences[0].AnnotationRequestPending())
    self.assertTrue(self.sentences[1].AnnotationRequestPending())
    self.assertFalse(self.sentences[2].AnnotationRequestPending())
    self.assertTrue(self.sentences[3].AnnotationRequestPending())


class TestOrganizingContentIntoParagraphs(utils.BasicSetupFixture):
  """Testing that we render the result organized into paragraphs.
    """

  def setUp(self):
    utils.BasicSetupFixture.setUp(self)

  def _PostRequestForArticle(self, req_content, n_sent=2):
    self.SubmitRequestViaPaypalAndMarkPaid(req_content, range(n_sent), 'BASIC')

  def _GetArticleHTML(self):
    resp = self.testapp.get('/dashboard')
    article_link = resp.html.find('a', {'class': 'article_link'})['href']
    return self.testapp.get(article_link).html

  def _GetTheArticleObject(self):
    return list(models.AnnotationRequest.query())[0]

  def testSingleLineChangeDoesNotProvideNewParagraphs(self):
    self._PostRequestForArticle('First sentence.\nSecond sentence.\n')
    the_article_html = self._GetArticleHTML()
    the_article = self._GetTheArticleObject()
    self.assertEquals(1, len(the_article.paragraphs))

  def testDoubleLineChangeProvidesNewParagraphs(self):
    self._PostRequestForArticle('First sentence.\n\nSecond sentence.\n')
    the_article = self._GetTheArticleObject()
    self.assertEquals(2, len(the_article.paragraphs))

  def testMoreThanTwoLineChangesProvideNewParagraphs(self):
    self._PostRequestForArticle('First sentence.\n \n \nSecond sentence.\n')
    the_article_html = self._GetArticleHTML()
    the_article = self._GetTheArticleObject()
    self.assertEquals(2, len(the_article.paragraphs))


class TestSpacing(utils.BasicSetupFixture):

  def setUp(self):
    utils.BasicSetupFixture.setUp(self)
    self.req_str = ("This request, has many special etc. characters. I'm "
                    'expecting it to be displayed correctly, with right '
                    'spacings between words.')
    self.req = self.SubmitRequestViaPaypalAndMarkPaid(self.req_str)

  def GetDisplayedText(self, spans):
    result_spans = []
    for i in spans:
      result_spans.extend(map(str, i.find_all(text=True)))
    return ' '.join(''.join(result_spans).strip().split())

  @unittest.skip("Temporarily disabled, and created a bug #43")
  def testSpacingCorrectWhenRenderingUnModifiedArticles(self):
    soup = self.testapp.get(self.req.GetURL()).html
    display_str = self.GetDisplayedText(soup.find_all('span',
                                                      {'class': 'sentence'}))
    self.assertEquals(self.req_str, display_str)

  @unittest.skip("Temporarily disabled, and created a bug #43")
  def testSpacingCorrectWhenCorrectionsAvailable(self):
    self.PostAnnotationFor(
      iter(self.req).next(), [{'s': 5,
                               'e': 7,
                               'revision': 'special'}])
    soup = self.testapp.get(self.req.GetURL()).html
    display_str = self.GetDisplayedText(soup.find_all('span',
                                                      {'class': 'sentence'}))
    self.assertEquals(
      "This request, has many special etc.special characters. I'm expecting "
      'it to be displayed correctly, with right spacings between words.',
      display_str)


class TestUserSpecifiedTitle(utils.BasicSetupFixture):

  def testUserSpecifiedTitle(self):
    self.SubmitRequestViaPaypalAndMarkPaid(
      'This is just a test request. It should be splitted up into two '
      'sentences.',
      [0, 1],
      'BASIC',
      title='Great article')
    self.assertIn('Great article', self.testapp.get('/dashboard').body)


if __name__ == '__main__':
  unittest.main()
