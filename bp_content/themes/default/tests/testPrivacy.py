import utils
import unittest
import os
import json
import main
from webtest import TestApp
from google.appengine.ext import testbed
from google.appengine.ext import ndb
from bp_content.themes.default import models


class TestUnableToAccessOtherPeopleDocuments(utils.BasicSetupFixture):

  def setUp(self):
    utils.BasicSetupFixture.setUp(self)

    self.req1 = self.SubmitRequestViaPaypal(
      "This is a testing request. This is the second sentence.")

    self.LoginAs('zbq')
    self.req2 = self.SubmitRequestViaPaypal(
      "Another user submitted a request.")

  def testUnableToAccessOtherPersonRequest(self):
    other_req_url = self.req1.GetURL()
    response = self.get(other_req_url, expect_errors=True)
    self.assertEquals(401, response.status_code)

  def testUnableToAccessNonExistingRequest(self):
    other_req_url = self.req1.GetURL()
    response = self.get('/article/non_existing', expect_errors=True)
    self.assertEquals(404, response.status_code)

  def testAccessDeniedForUnloggedinUsers(self):
    other_req_url = self.req1.GetURL()
    response = self.get(other_req_url, expect_errors=True)
    self.assertEquals(401, response.status_code)

    response = self.get('/article/non_existing', expect_errors=True)
    self.assertEquals(404, response.status_code)

  def testAbleToAceessOwnRequest(self):
    own_req_url = self.req2.GetURL()
    response = self.get(own_req_url)
    self.assertEquals(200, response.status_code)
