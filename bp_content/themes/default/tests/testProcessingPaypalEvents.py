import utils
import unittest
import os
import json
from webtest import TestApp
from google.appengine.ext import testbed
from google.appengine.ext import ndb

from bp_content.themes.default.handlers import handlers
from bp_content.themes.default import models


class TestSubmittingRequestsOnPaymentSuccesses(utils.BasicSetupFixture):

  def setUp(self):
    utils.BasicSetupFixture.setUp(self)
    self.req = self.SubmitRequestViaPaypal(
      "This is a testing request. This is the second sentence.")
    self.assertEquals(0, len(list(iter(self.req))))

  def testSubmittingAllRequestsToTaskQueue(self):
    handlers.PaypalIPNHandler.HandleVerifiedRequest(
      {'custom': self.req.key.urlsafe(),
       'payment_status': 'Completed'},
    )
    self.execute_tasks(expect_tasks=1)

    self.sent1_req, self.sent2_req = list(iter(self.req))

    tasks = self.taskqueue_stub.get_filtered_tasks()
    self.assertEquals(2, len(tasks))
    self.assertEquals('/taskqueue_submit_turker_request', tasks[0].url)
    self.assertEquals('/taskqueue_submit_turker_request', tasks[1].url)
    self.assertEquals(
      self.sent1_req.key.urlsafe(), tasks[0].extract_params()['request_key'])
    self.assertEquals(
      self.sent2_req.key.urlsafe(), tasks[1].extract_params()['request_key'])

  def testNotSubmittingWhenPaymentPending(self):
    handlers.PaypalIPNHandler.HandleVerifiedRequest(
      {'custom': self.req.key.urlsafe(),
       'payment_status': 'Pending'},
    )
    self.execute_tasks(expect_tasks=0)

    self.assertEquals(0, len(list(iter(self.req))))

  def testNoDuplicateSubmissionAttempts(self):
    handlers.PaypalIPNHandler.HandleVerifiedRequest(
      {'custom': self.req.key.urlsafe(),
       'payment_status': 'Completed'},
    )
    self.execute_tasks(expect_tasks=1)
    self.ClearTaskQueueRequests()

    handlers.PaypalIPNHandler.HandleVerifiedRequest(
      {'custom': self.req.key.urlsafe(),
       'payment_status': 'Completed'},
    )
    self.execute_tasks(expect_tasks=0)
