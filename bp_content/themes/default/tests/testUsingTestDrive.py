from bp_content.themes.default import settings
from bp_content.themes.default import models
from bp_content.themes.default.tests import utils


class TestUserTestDriveWorkflow(utils.BasicSetupFixture):

  def SetupUser(self):
    # Do not login
    pass

  def SubmitRegistrationForm(
      self,
      email='abc@gmail.com',
      password='abc',
      c_password=None,
      title='Testing article',
      content='Testing sentence #1.\n\nTesting sentence #2.',
      expect_error_message=None):
    form = self.get_form('/testdrive/', 'form_testdrive')
    form['email'] = email
    form['password'] = password
    if c_password is None:
      form['c_password'] = password
    else:
      form['c_password'] = c_password

    form['title'] = title
    form['content'] = content

    expect_error = (expect_error_message is not None)

    self.submit(form, expect_error=expect_error,
                error_message=expect_error_message)

  def GetTheAnnotationRequest(self):
    requests = models.AnnotationRequest.query().fetch(2)
    self.assertEqual(1, len(requests))
    request = requests[0]
    return request

  def testWalkingThroughWholeProcessNormally(self):
    # First registering the user
    self.SubmitRegistrationForm()

    user = self.GetUser(email='abc@gmail.com')
    # Right after account creation, no requests sent out yet
    request = self.GetTheAnnotationRequest()
    self.assertEqual('CREATED', request.stage)
    # One email.
    self.assertEqual(1, len(self.taskqueue_stub.get_filtered_tasks()))

    # After activating the user, the sentences are submitted
    self.activate_user(user)
    request = self.GetTheAnnotationRequest()

    self.assertEqual('SUBMITTED', request.stage)
    # Two sentences.
    self.assertEqual(2, len(self.taskqueue_stub.get_filtered_tasks()))

  def testMoreThanSentenceLimit(self):
    limit = settings.TESTDRIVE_MAX_SENT
    self.SubmitRegistrationForm(
      content=" ".join(
        'This is sentence number %d. ' %
        i for i in range(
          limit + 1)),
      expect_error_message='Number of sentences must be under %d' % limit)

  def testUserLoginAfterTestDriveActivation(self):
    self.SubmitRegistrationForm()
    user = self.GetUser(email='abc@gmail.com')
    self.activate_user(user)
    self.assert_user_logged_in(user.get_id())

    result_html = str(self.get('/dashboard').html)
    self.assertIn('Logout', result_html)
    self.assertNotIn('Login', result_html)

  def testUserSeesNewArticleWhenRegistered(self):
    self.SubmitRegistrationForm()
    user = self.GetUser(email='abc@gmail.com')
    self.activate_user(user)

    result_html = str(self.get('/intro').html)
    self.assertIn('Paste', result_html)
    self.assertNotIn('Free', result_html)
