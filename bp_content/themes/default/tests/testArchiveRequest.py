import utils
import unittest
import os
import json
import main
from webtest import TestApp
from google.appengine.ext import testbed
from google.appengine.ext import ndb
from bp_content.themes.default import models


class TestArchivingRequests(utils.BasicSetupFixture):

  def setUp(self):
    utils.BasicSetupFixture.setUp(self)

    self.req1 = self.SubmitRequestViaPaypal(
      "Request #1. This is the second sentence.")
    self.req2 = self.SubmitRequestViaPaypal(
      "Request #2. This is the second sentence.")

  def testDisappearOnDashboardWhenMarkArchive(self):
    self.get(self.req1.GetArchiveUrl(), status=302)
    self.assertNotIn("#1", str(self.get("/dashboard").html))
    self.assertIn("#2", str(self.get("/dashboard").html))

  def testCannotArchiveOtherPersonArticle(self):
    self.LoginAs('zbq')
    response = self.get(self.req1.GetArchiveUrl(), expect_errors=True)
    self.assertEquals(401, response.status_code)
