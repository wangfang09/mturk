import utils
import unittest
import os
import json
import main
import logging
from webtest import TestApp
from google.appengine.ext import testbed
from google.appengine.ext import ndb
from bp_content.themes.default import models


class TestAbleToAccessAllContent(utils.BasicSetupFixture):

  def testAccessingRoot(self):
    self.testapp.get('/')

  def FineURL(self, url):
    return 'testbed.example.com' in url or 'mailto:' in url

  def TraverseFrom(self, url, prev_url, visited_urls):
    if url is None or url.startswith('#') or self.FineURL(url):
      return
    if url in visited_urls:
      return
    visited_urls.add(url)
    try:
      resp = self.testapp.get(url)
    except:
      logging.error("Error accessing %s from %s", url, prev_url)
      raise
    while 300 <= resp.status_int < 400:
      try:
        resp = resp.follow()
      except Exception:
        raise ValueError(resp)
    content = resp.html
    for l in content.find_all('a'):
      self.TraverseFrom(l.get('href'), url, visited_urls)

  def testTraversingWholeSite(self):
    self.TraverseFrom('/', '/', set())


class TestAbleToAccessContentAfterSomeSubmissions(TestAbleToAccessAllContent):

  def setUp(self):
    TestAbleToAccessAllContent.setUp(self)
    self.SubmitRequestViaPaypal("First sentence. Second sentence.")


class TestUnloggedInUserAbleToAccessContent(TestAbleToAccessAllContent):

  def SetupUser(self):
    pass

  def FineURL(self, url):
    return TestAbleToAccessAllContent.FineURL(
      self, url) or url.endswith('/dashboard')

if __name__ == "__main__":
  unittest.main()
