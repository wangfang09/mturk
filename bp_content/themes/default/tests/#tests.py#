import unittest
import os
import json
import main
import settings
from webtest import TestApp
from google.appengine.ext import testbed
from google.appengine.ext import ndb
import models

class BasicSetupFixture(unittest.TestCase):

    def setUp(self):        
        os.environ['APPLICATION_ID'] = 'mturk-revisor'
        self.app = TestApp(main.app)
        
        self.testbed = testbed.Testbed()
        self.testbed.setup_env(
            current_version_id='testbed.version',
            USER_EMAIL="xhc@hotmail.com",
            USER_ID="1",
            USER_IS_ADMIN='1',
        ) 
        self.testbed.activate()
        self.testbed.init_datastore_v3_stub()
        self.testbed.init_user_stub()
        self.testbed.init_memcache_stub()

    def tearDown(self):
        self.testbed.deactivate()


class TestSubmitRequest(BasicSetupFixture):

    def setUp(self):
        BasicSetupFixture.setUp(self)
        resp = self.app.post(
            '/add_article', {
                'content' :
                'This is just a test request. It should be splitted up into two sentences.'})
        build_req_page = resp.follow()
        build_req_page.form['activate_list'].value = json.dumps([0, 1])
        build_req_page.form.submit()

    def testShowArticleDashboard(self):
        self.assertIn('This is just', self.app.get('/').body)

    def testHaveOpenRequests(self):
        requests = models.SentenceAnnotationRequest.GetOpenRequests()
        self.assertEquals(2, len(requests))
        self.assertIn('request', requests[0].GetSentence().words)

    def testTurkAnnotateRequestWithNativeInterface(self):
        soup = self.app.get('/turk').html
        annotation_page = self.app.get(
            soup.find('a', {'class' : 'req_link'})[
                'href'])
        annotation_page.form['annotation'] = json.dumps([{'s' : 2, 'e' : 2, 'revision' : ''}])
        annotation_page.form.submit()

        suggestions = list(models.SentenceRevisionSuggestion.query())
        self.assertEquals(1, len(suggestions))
        self.assertEquals(
            (2, 2, ''),
            (suggestions[0].start,
             suggestions[0].end,
             suggestions[0].revise_into))


class TestDisplayingSentencesAndAnnotations(
        BasicSetupFixture):

    def setUp(self):
        BasicSetupFixture.setUp(self)
        self.user = models.User.GetCurrentUser()
        self.article = self.user.AddArticle(
            "This is the first sentence. And this is the second sentence.")
        self.article.MakeSurePreprocessed()
        self.assertEquals(
            2, len(self.article.GetSentences()))
        self.request = self.article.AddAnnotationRequest([0, 1])
        self.sent_requests = list(iter(self.request))
        self.sent_requests[0].SetSuggestions([(4, 4, "")])
        self.sent_requests[1].SetSuggestions([(0, 0, ""), (5, 5, "one")])

    def testGettingSentenceAndAnnotations(self):
        self.assertEquals(
            [[("This is the first", None),
              ("sentence", ""),
              (".", None),],
             [("And", ""),
              ("this is the second", None),
              ("sentence", "one"),
              (".", None)]],
            list(self.article.GetSentencesAndAnnotations()))


class TestOrganizingContentIntoParagraphs(BasicSetupFixture):
    """Testing that we render the result organized into paragraphs.
    """
    def setUp(self):
        BasicSetupFixture.setUp(self)

    def _PostRequestForArticle(self, req_content, n_sent=2):
        resp = self.app.post('/add_article', {'content' : req_content})
        build_req_page = resp.follow()
        build_req_page.form['activate_list'].value = json.dumps(range(n_sent))
        build_req_page.form.submit()

    def _GetArticleHTML(self):
        resp = self.app.get('/')
        article_link = resp.html.find('a', {'class':'article_link'})['href']
        return self.app.get(article_link).html
        
    def testSingleLineChangeDoesNotProvideNewParagraphs(self):
        self._PostRequestForArticle("First sentence.\nSecond sentence.\n")
        the_article_html = self._GetArticleHTML()
        self.assertEquals(1, len(the_article_html.find_all('p', {'class' : 'paragraph'})))

    def testDoubleLineChangeProvidesNewParagraphs(self):
        self._PostRequestForArticle("First sentence.\n\nSecond sentence.\n")
        the_article_html = self._GetArticleHTML()
        self.assertEquals(2, len(the_article_html.find_all('p', {'class' : 'paragraph'})))

    def testMoreThanTwoLineChangesProvideNewParagraphs(self):
        self._PostRequestForArticle("First sentence.\n \n \nSecond sentence.\n")
        the_article_html = self._GetArticleHTML()
        self.assertEquals(2, len(the_article_html.find_all('p', {'class' : 'paragraph'})))


@ipn.Register
class IPNRecordHandler(ipn.IPNHandlerBase):
    def __init__(self, *args, **kw):
        ipn.IPNHandlerBase.__init__(self, *args, **kw)
        self.records = []
        self.verified = False

    def SetVerified(self, v):
        self.verified = v

    def OnVerified(self, trans_msg):
        self.records.append(('verified', trans_msg))

    def OnUnVerified(self, trans_msg):
        self.records.append(('unverified', trans_msg))

    def PostMessage(self, trans_msg):
        self.records.append(('posted', trans_msg))
        return self.verified ? 'VERIFIED' : 'UNVERIFIED'

    def GetRecording(self):
        return self.records
    

class TestPaypalIPN(BasicSetupFixture):
    
    def setUp(self):
        self.ipn_handler_setting_backup = settings.IPNHandlerName
        BasicSetupFixture.setUp(self)

    def tearDown(self):
        BasicSetupFixture.tearDown(self)
        settings.IPNHandlerName = self.ipn_handler_setting_backup

    def testServerHandlesCommunicationsAllRight(self):
        settings.IPNHandlerName = 'IPNRecordHandler'
        resp = self.app.post('/ipn', {'SomePayPalVar' : 'SomeValue1',
                                      'SomeOtherPPVar' : 'SomeValue2'})
        self.assertEquals(200, resp.response_code)
        self.assertEquals("", resp.html)

        self.assert
        


if __name__ == "__main__":
    unittest.main()        
