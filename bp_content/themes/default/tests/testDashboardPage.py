import utils
import unittest
import os
import json
import main
from webtest import TestApp
from google.appengine.ext import testbed
from google.appengine.ext import ndb

from bp_content.themes.default import models


class TestStatusTracking(utils.BasicSetupFixture):

  def testShowPaymentLinkWhenNotSubmitted(self):
    req = self.SubmitRequestViaPaypal(
      'Sentence one is short. Sentence two is also not long. ')

    result = self.testapp.get('/dashboard').html
    self.assertIn('payment pending', str(result).lower())
    self.assertNotIn('0/2', str(result))

    self.assertIn('paypal', str(self.testapp.get(req.GetURL()).html).lower())

  def testProgressBarJSInplaceWhenPaymentSubmitted(self):
    req = self.SubmitRequestViaPaypalAndMarkPaid(
      'Sentence one is short. Sentence two is also not long. ')

    result = self.testapp.get('/dashboard').html
    self.assertNotIn('pending', str(result))
    self.assertIn(req.key.urlsafe(), str(result))

    self.assertNotIn('paypal', str(self.testapp.get(req.GetURL()).html))

  def testShowCompletedWhenAllIn(self):
    req = self.SubmitRequestViaPaypalAndMarkPaid(
      'Sentence one is short. Sentence two is also not long. ')
    self.assertFalse(req.GetRenderingData().AllFinished())
    req1, req2 = tuple(iter(req))
    self.PostAnnotationFor(req1, [{'s': 2, 'e': 2, 'revision': 'is very'}])
    self.PostAnnotationFor(req2, [{'s': 2, 'e': 2, 'revision': 'is very'}])
    self.assertTrue(req.GetRenderingData().AllFinished())
    result = self.testapp.get('/dashboard').html
    self.assertIn('Completed', str(result))
