import unittest
from bp_content.themes.default import string_utils


class TestJoinWordsWithSpace(unittest.TestCase):

  def testJoiningWithoutWords(self):
    self.assertEquals("", string_utils.JoinWordsWithSpace([]))

  def testJoiningOneWord(self):
    self.assertEquals("test", string_utils.JoinWordsWithSpace(["test"]))

  def testHasPeriod(self):
    self.assertEquals("test.",
                      string_utils.JoinWordsWithSpace(["test", "."]))

  def testNormalSentence(self):
    self.assertEquals(
      "I love you.",
      string_utils.JoinWordsWithSpace(["I", "love", "you", "."]))

  def testIam(self):
    self.assertEquals(
      "I'm good.",
      string_utils.JoinWordsWithSpace(["I", "'m", "good", "."]))

  def testHasEmptyStrings(self):
    self.assertEquals("I'm good.", string_utils.JoinWordsWithSpace(
      ["I", "", "'m", "", "good", "", "."]))

  def testDoubleQuotes(self):
    self.assertEquals(
      "entitled ``Shifting the Instructional Focus to the Learner'' and",
      string_utils.JoinWordsWithSpace(
        "entitled `` Shifting the Instructional Focus to the Learner '' and".split(
        )))

  def testNAT(self):
    self.assertEquals(
      "They didn't go.",
      string_utils.JoinWordsWithSpace("They did n't go .".split()))
