import utils
import unittest
import os
import json
import main
import mock
from webtest import TestApp
from google.appengine.ext import testbed
from google.appengine.ext import ndb
from bp_content.themes.default import turk
from bp_content.themes.default import models


class TestAdminAbleToViewAllArticlesAndMarkThemPaid(utils.BasicSetupFixture):

  def setUp(self):
    utils.BasicSetupFixture.setUp(self)

    self.req1 = self.SubmitRequestViaPaypal(
      "This is a testing request. This is the second sentence.", title="article 1")

    self.LoginAs('zbq')
    self.req2 = self.SubmitRequestViaPaypal(
      "Another user submitted a request.", title="article 2")

    self.LoginAsAdmin()

  def tearDown(self):
    self.Logout()

  def testAbleToSeeAllArticlesAndAuthors(self):
    articles_listing_page = self.get("/admin/articles/").html
    self.assertIn('zbq', str(articles_listing_page))
    self.assertIn('xhc', str(articles_listing_page))

    self.assertIn('article 1', str(articles_listing_page))
    self.assertIn('article 2', str(articles_listing_page))

  def testAbleToViewArticleContent(self):
    url = self.uri_for(
      'admin-request-view',
      article_urlsafe=self.req1.key.urlsafe())
    article_page = self.get(url).html
    self.assertIn("This is a testing request", str(article_page))

  def testAbleMarkAsPaid(self):
    self.req1.MarkAsPaid = mock.Mock()
    url = self.uri_for(
      'admin-mark-as-paid',
      order_urlsafe=self.req1.key.urlsafe())
    self.get(url)
    self.execute_tasks()

    self.assertTrue(self.req1.MarkAsPaid.called)

  def testExpiringHits(self):
    self.called_time = 0

    def CustomCreateHit(*args, **kw):
      return_obj = mock.Mock()
      self.called_time += 1
      if self.called_time == 1:
        return_obj.HITId = "1"
      elif self.called_time == 2:
        return_obj.HITId = "2"
      return [return_obj]

    turk_config = mock.Mock()
    turk_config.CreateHit = CustomCreateHit
    turk.TurkConfig.the_config = turk_config

    self.req1.MarkAsPaid()
    self.execute_tasks(expect_tasks=2)

    url = self.uri_for(
      'admin-expire-requests',
      article_urlsafe=self.req1.key.urlsafe())
    self.get(url, status=302)
    self.execute_tasks(expect_tasks=2)

    self.assertEquals([(("1",), {}), (("2",), {})],
                      turk_config.ExpireHit.call_args_list)
