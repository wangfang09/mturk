import unittest
import logging
from webtest import app
from bp_content.themes.default.tests import utils


class TestTrackArticleProgressAPI(utils.BasicSetupFixture):

  def setUp(self):
    utils.BasicSetupFixture.setUp(self)

    self.request = self.SubmitRequestViaPaypalAndMarkPaid(
      "In this test, I am planning to first submit corrections for"
      " one sentence, and then two sentences. Also I will test tracking "
      " progress for non-existing sentences.")
    self.request_urlsafe = self.request.key.urlsafe()

  def GetTrackingResult(self, request_urlsafe=None):
    if request_urlsafe is None:
      request_urlsafe = self.request_urlsafe
    response = self.CallMethod(
      "get_progress", {'article_urlsafe': request_urlsafe}
    )
    return response

  def testZeroCompletionByDefault(self):
    result = self.GetTrackingResult().json
    self.assertEquals(0, int(result['n_finished']))
    self.assertEquals(2, int(result['n_total']))

  def testOneFinished(self):
    req1, req2 = tuple(iter(self.request))
    self.PostAnnotationFor(req1, [{'s': 0, 'e': 3, 'revision': ''}])

    result = self.GetTrackingResult().json
    self.assertEquals(1, int(result['n_finished']))
    self.assertEquals(2, int(result['n_total']))

  def testErrorOnNonExistingArticle(self):
    self.assertRaises(
      app.AppError, self.GetTrackingResult, "non_existing_article_urlsafe")

  @unittest.skip(
    "Currently don't know how to use authentication in endpoints. ")
  def testOtherPeopleArticle(self):
    self.LoginAs('zbq')
    self.assertRaises(app.AppError, self.GetTrackingResult)

  def testGettingRevisionResult(self):
    requests = list(iter(self.request))
    self.PostAnnotationFor(requests[0], [{'s': 0, 'e': 3, 'revision': ''}])
    response = self.CallMethod(
      "get_result", {'article_urlsafe': self.request_urlsafe})
    logging.info("Actual response: %s", response)

    sentences = response.json['paragraphs'][0]['sentences']
    first_sentence = sentences[0]
    second_sentence = sentences[1]
    first_span = first_sentence['annotation_spans'][0]

    self.assertEquals("In this test,", first_span['original_words'])
    self.assertEquals("", first_span['revise_into'])
    self.assertFalse(first_sentence['annotation_request_pending'])
    self.assertTrue(first_sentence['finished_annotation'])

    self.assertEquals("In this test,", first_span['original_words'])
    self.assertEquals("", first_span['revise_into'])

    self.assertFalse(first_sentence['annotation_request_pending'])
    self.assertTrue(first_sentence['finished_annotation'])

    self.assertTrue(second_sentence['annotation_request_pending'])
    self.assertFalse(second_sentence['finished_annotation'])
