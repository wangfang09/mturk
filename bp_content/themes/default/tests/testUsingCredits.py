import utils
import unittest
import os
import json
import main

from webtest import TestApp
from google.appengine.ext import testbed
from google.appengine.ext import ndb
from bp_content.themes.default import models
from bp_content.themes.default import settings


class TestUsingCredits(utils.BasicSetupFixture):

  def setUp(self):
    utils.BasicSetupFixture.setUp(self)
    self._prem_price_backup = settings.USER_CHARGE_CREDITS['PREMIUM']
    self.LoginAs('zbq')

  def tearDown(self):
    utils.BasicSetupFixture.tearDown(self)
    settings.USER_CHARGE_CREDITS['PREMIUM'] = self._prem_price_backup

  def testPlacingCreditsOrderResultsInDBRecord(self):
    form = self.get_form("/add_credits/", "form_add_credits")
    form["credits"] = "500"
    redirection_page = self.submit(form)
    order = ndb.Key(urlsafe=redirection_page.form['custom'].value).get()

    self.assertAlmostEquals(5.30, order.amount)
    self.assertEquals('CREATED', order.stage)
    self.assertIn('zbq', order.user.get().GetNickname())
    self.assertEquals(500, order.n_credits)

  def testPayingForCreditsResultsInCreditsIncrease(self):
    cur_user = models.User.get_by_email('zbq@hotmail.com')
    self.assertEquals(200, cur_user.credits)

    form = self.get_form("/add_credits/", "form_add_credits")
    form["credits"] = "500"
    redirection_page = self.submit(form)
    order = ndb.Key(urlsafe=redirection_page.form['custom'].value).get()
    order.MarkAsPaid()

    cur_user = models.User.get_by_email('zbq@hotmail.com')
    self.assertEquals(700, cur_user.credits)
    self.assertEquals('SUBMITTED', order.stage)

  def testSubmittingSentencesWithCredits(self):
    cur_user = models.User.get_by_email('zbq@hotmail.com')
    self.assertEquals(200, cur_user.credits)
    request = self.SubmitRequestUsingCredits(
      "This is the only sentence.", price_choice='PRIORITY')

    tasks = self.taskqueue_stub.get_filtered_tasks()
    self.assertEquals(1, len(tasks))
    self.assertEquals('/taskqueue_mark_order_as_paid/', tasks[0].url)

    cur_user = models.User.get_by_email('zbq@hotmail.com')
    self.assertEquals(196, cur_user.credits)

  def testSubmittingSentencesButNotEnoughCredits(self):
    # Money isn't enough, coz it would need $2.5 worth, while they only have
    # $2.
    content = " ".join(("This is the %dth sentence." % i) for i in range(5))
    settings.USER_CHARGE_CREDITS['PREMIUM'] = 50

    request = self.SubmitRequestUsingCredits(
      content, price_choice="PREMIUM", expect_error=True, error_message='Insufficient Credits')
    cur_user = models.User.get_by_email('zbq@hotmail.com')
    self.assertEquals(200, cur_user.credits)
    self.assertEquals([], self.taskqueue_stub.get_filtered_tasks())
    self.assertEquals("CREATED", request.stage)
