import utils
import unittest
import os
import json
from webtest import TestApp
from google.appengine.ext import testbed
from google.appengine.ext import ndb
import mock

from bp_content.themes.default import models
from bp_content.themes.default import turk
import main


class TestSQSInteractions(utils.BasicSetupFixture):

  def setUp(self):
    utils.BasicSetupFixture.setUp(self)
    self.article_request = self.SubmitRequestViaPaypalAndMarkPaid(
      'This is just a test request. And we have the second sentence.')

    self.hitid_counter = 0

    self.mock_config = mock.Mock()

    self.mock_config.CreateHit = self.HitIdCreater
    turk.TurkConfig.the_config = self.mock_config

    self.execute_tasks(expect_tasks=2)

  def tearDown(self):
    turk.TurkConfig.the_config = None
    utils.BasicSetupFixture.tearDown(self)

  def HitIdCreater(self, *args, **kw):
    self.hitid_counter += 1
    mock_hit_obj = mock.Mock()
    mock_hit_obj.HITId = str(self.hitid_counter)
    return [mock_hit_obj]

  def SetSQSContent(self, content):
    self.mock_config.GetAvailableHitsInQueue.return_value = content

  def UpdateSQS(self):
    self.testapp.get('/admin/crontasks/update_sqs')

  def testUpdatingOnExistingID(self):
    self.SetSQSContent(["1"])
    self.mock_config.GetHitAnnotation.return_value = json.dumps(
      [{"s": 2, "e": 2, "revision": ""}])
    self.UpdateSQS()

    self.assertEquals((("1", ),), self.mock_config.GetHitAnnotation.call_args)
    self.assertEquals(2, list(iter(self.article_request))[0].result[0].start)

  def testSQSContainsNonExistingID(self):
    self.SetSQSContent(["3"])
    self.UpdateSQS()
    self.assertEquals([], self.mock_config.GetHitAnnotation.call_args_list)


class TestCreateHITFailures(utils.BasicSetupFixture):

  def setUp(self):
    utils.BasicSetupFixture.setUp(self)
    self.article_request = self.SubmitRequestViaPaypalAndMarkPaid(
      'This is just a test request. And we have the second sentence.')

    self.mock_config = mock.Mock()
    turk.TurkConfig.the_config = self.mock_config

  def tearDown(self):
    turk.TurkConfig.the_config = None
    utils.BasicSetupFixture.tearDown(self)

  def _RaiseErrorWhenCreatingHit(self, *args, **kw):
    raise RuntimeError()

  def testNotGenerateSentenceRequestWhenCreationFailure(self):
    self.mock_config.CreateHit = self._RaiseErrorWhenCreatingHit

    for task in self.taskqueue_stub.get_filtered_tasks():
      self.testapp.post(task.url, task.extract_params())

    req1, req2 = list(iter(self.article_request))
    self.assertEquals('SUBMISSION_ERROR', req1.phase)
    self.assertEquals('SUBMISSION_ERROR', req2.phase)
    self.assertIsNone(req1.hit_id)
    self.assertIsNone(req2.hit_id)
