import unittest
import os
import sys
import main
import webapp2
import json
import logging
from webtest import TestApp
from google.appengine.ext import testbed
from google.appengine.ext import ndb
sys.path.append('/usr/local/google_appengine')
sys.path.append('/usr/local/google_appengine/lib/yaml/lib')
from webtest import TestApp
import endpoints
import services
from bp_includes.lib import test_helpers
from bp_includes import routes as routes_boilerplate
from bp_content.themes.default import routes as routes_theme
from bp_content.themes.default import models
from bp_includes import config as config_boilerplate
from bp_content.themes.default import config as config_theme
from bp_admin import routes as routes_admin
from bp_includes.lib import utils as bp_utils


class BasicSetupFixture(unittest.TestCase, test_helpers.HandlerHelpers):

  def setUp(self):
    os.environ['APPLICATION_ID'] = 'mturk-revisor'

    webapp2_config = config_boilerplate.config
    webapp2_config.update(config_theme.config)
    self.app = webapp2.WSGIApplication(config=webapp2_config, debug=True)
    routes_theme.add_routes(self.app)
    routes_boilerplate.add_routes(self.app)
    routes_admin.add_routes(self.app)

    self.testapp = TestApp(
      main.app, extra_environ={
        'REMOTE_ADDR': '127.0.0.1'})

    self.testbed = testbed.Testbed()
    self.testbed.setup_env(current_version_id='testbed.version')
    self.testbed.activate()
    self.testbed.init_datastore_v3_stub()
    self.testbed.init_user_stub()
    self.testbed.init_memcache_stub()
    self.testbed.init_urlfetch_stub()
    self.testbed.init_mail_stub()
    self.testbed.init_taskqueue_stub()
    self.mail_stub = self.testbed.get_stub(testbed.MAIL_SERVICE_NAME)
    self.taskqueue_stub = self.testbed.get_stub(testbed.TASKQUEUE_SERVICE_NAME)

    self.headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_4) Version/6.0 Safari/536.25',
                    'Accept-Language': 'en_US'}
    if not bp_utils.is_email_valid(self.app.config.get('contact_sender')):
      self.app.config['contact_sender'] = "noreply-testapp@example.com"
    if not bp_utils.is_email_valid(self.app.config.get('contact_recipient')):
      self.app.config['contact_recipient'] = "support-testapp@example.com"

    xhc = self.register_user('xhc', '123456', 'xhc@hotmail.com')
    self.activate_user(xhc)
    self.testapp.reset()
    zbq = self.register_user('zbq', '234567', 'zbq@hotmail.com')
    self.activate_user(zbq)
    self.testapp.reset()

    self.SetupUser()

  def tearDown(self):
    self.Logout()
    self.testbed.deactivate()

  def uri_for(self, name, *args, **kw):
    return self.app.router.build('/', name, args, kw)

  def CallMethod(self, methodname, args={}):
    logging.info("Calling %s with %r", methodname, args)
    result = self.testapp.post_json('/_api/%s' % (methodname), args)
    logging.info("Got result: %r", result)
    return result

  def SubmitRequest(
      self, payment_type, content, activate_list=None, price_choice="PRIORITY", title='',
      expect_error=False, error_message=None):
    paragraphs = self.CallMethod('preprocess', {'content': content})
    if activate_list is None:
      total_sent = sum(len(par['sentences'])
                       for par in paragraphs.json['paragraphs'])
      activate_list = range(total_sent)

    form = self.get_form('/add_request/', 'form_add_request')
    form['title'] = title
    form['content'] = content
    form['paragraphs'] = json.dumps(paragraphs.json)
    form['price_choice'] = price_choice
    form['activate_list'] = json.dumps(activate_list)
    form['payment_type'] = payment_type
    redirection_page = self.submit(
      form,
      expect_error=expect_error,
      error_message=error_message)
    return redirection_page

  def SubmitRequestUsingCredits(self, *args, **kw):
    redirection_page = self.SubmitRequest('credits', *args, **kw)
    request = models.AnnotationRequest.query().order(
      -models.AnnotationRequest.creation_timestamp).fetch(1)[0]
    return request

  def SubmitRequestViaPaypal(self, *args, **kw):
    redirection_page = self.SubmitRequest('paypal', *args, **kw)
    request = ndb.Key(urlsafe=redirection_page.form['custom'].value).get()
    return request

  def PostAnnotationFor(self, sent_req, suggestions):
    form = self.get_form(sent_req.GetAnnotateURL(), 'form')
    form['annotation'] = json.dumps(suggestions)
    form['request_urlsafe'] = sent_req.key.urlsafe()
    self.submit(form)

  def SubmitRequestViaPaypalAndMarkPaid(self, *args, **kw):
    request = self.SubmitRequestViaPaypal(*args, **kw)
    request.MarkAsPaid()

    return request

  def SetupUser(self):
    self.LoginAs('xhc')

  def ClearTaskQueueRequests(self):
    tasks = self.taskqueue_stub.get_filtered_tasks()
    for task in tasks:
      self.taskqueue_stub.DeleteTask('default', task.name)

  def LoginAs(self, username):
    self.testapp.reset()
    if username == 'xhc':
      self.login_user('xhc', '123456')
    elif username == 'zbq':
      self.login_user('zbq', '234567')
    else:
      raise ValueError()

  def Logout(self):
    self.get("/logout", status=301)
    self.testbed.setup_env(
      user_email='', user_id='', user_is_admin='0', overwrite=True)

  def LoginAsAdmin(self):
    self.Logout()
    self.testbed.setup_env(
      user_email='xxx@gmail.com',
      user_id='xxx',
      user_is_admin='1',
      overwrite=True)

  def GetUser(self, email=None):
    if email is not None:
      users = models.User.query(models.User.email == email).fetch(2)
    self.assertEqual(1, len(users), "Could not register user abc@gmail.com")
    return users[0]
