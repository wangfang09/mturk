var add_article_app = angular.module(
  'AddArticleApp', []);

var MAX_N_WORDS = 5;

add_article_app.config(
  ['$interpolateProvider', function($interpolateProvider) {
    $interpolateProvider.startSymbol('{[');
    $interpolateProvider.endSymbol(']}');
  }]);

add_article_app.controller(
  'AddArticleCtrl',
  function($scope) {
    $scope.title = "";
    $scope.content = "";

    $scope.self_defined_title = false;

    $scope.UpdateTitle = function() {
      if (!$scope.self_defined_title) {
        if ($scope.content.split(" ").length > MAX_N_WORDS) {
          $scope.title = $scope.content.split(" ").splice(
            0, MAX_N_WORDS).join(" ").concat("...");
        } else {
          $scope.title = $scope.content;
        }
      }
    }

    $scope.DefinedTitle = function() {
      $scope.self_defined_title = true;
    }
  });