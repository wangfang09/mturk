function init() {
  window.setTimeout(window.init, 100);
}

var add_request_app = angular.module(
  'AddRequestApp', ['ui.bootstrap']);

var MAX_N_WORDS = 5;

var PRICE_MAP = {
  "BASIC": 3,
  "PRIORITY": 4,
  "PREMIUM": 5,
};

add_request_app.config(
  ['$interpolateProvider', function($interpolateProvider) {
    $interpolateProvider.startSymbol('{[');
    $interpolateProvider.endSymbol(']}');
  }]);

add_request_app.factory('WordContentPreprocessing', function($location, $http) {
  return {
    'Preproc': function(content, call_back) {
      $http.post("/_api/preprocess", JSON.stringify({
        'content': content
      })).then(call_back);
    },
    'GetUserInfo': function(user_urlsafe, call_back) {
      $http.post("/_api/get_user_info", JSON.stringify({
        'user_urlsafe': user_urlsafe
      })).then(call_back);
    },
  }
});

add_request_app.factory('ArgumentLoading', function() {
  return {
    'GetUserURLSafe': function() {
      return document.getElementById("user_urlsafe").innerHTML;
    }
  }
})

add_request_app.controller('AddRequestCtrl', function(
  $scope, $window, WordContentPreprocessing, ArgumentLoading) {
  $scope.init_func = function() {
    $scope.user_urlsafe = ArgumentLoading.GetUserURLSafe();

    $scope.title = "";
    $scope.content = "";
    $scope.second_step_not_ready = true;
    $scope.not_loading = true;

    $scope.paragraphs = [];
    $scope.sentences = [];

    $scope.request = null;
    $scope.min_sent_len = 10;
    $scope.price_choice = 'BASIC';
    $scope.current_credits = 0.0;
    $scope.total_credits = 0.0;
    $scope.credits_after = 0.0;
    $scope.self_defined_title = false;

    $scope.enough_credits = true;
    $scope.balance_reloading = false;

    $scope.CalculateTotalCredits = function() {
      var total_activated = 0;
      for (i in $scope.sentences) {
        if ($scope.sentences[i].activated) {
          total_activated += 1;
        }
      }
      return PRICE_MAP[$scope.price_choice] * total_activated;
    };

    $scope.UpdateTotalPrice = function() {
      var total_credits = $scope.CalculateTotalCredits();
      $scope.total_credits = total_credits;
      $scope.credits_after = $scope.current_credits - $scope.total_credits;
      $scope.enough_credits = ($scope.credits_after >= 0);
    };

    $scope.balance_reloading = true;
    WordContentPreprocessing.GetUserInfo($scope.user_urlsafe, function(resp) {
      $scope.current_credits = resp.data.credits;
      $scope.balance_reloading = false;
      $scope.UpdateTotalPrice();
    })

    function UpdateTitle() {
      if (!$scope.self_defined_title) {
        if ($scope.content.split(" ").length > MAX_N_WORDS) {
          $scope.title = $scope.content.split(" ").splice(
            0, MAX_N_WORDS).join(" ").concat("...");
        } else {
          $scope.title = $scope.content;
        }
      }
    }

    $scope.OnContentEdit = function() {
      UpdateTitle();
      $scope.BackToStepOne();
    }

    $scope.DefinedTitle = function() {
      $scope.self_defined_title = true;
    }

    $scope.ToggleActivation = function(ind) {
      $scope.sentences[ind].activated = !$scope.sentences[ind].activated;
      $scope.sentences[ind].determined = true;
      $scope.UpdateRequest();
      $scope.UpdateTotalPrice();
    };

    $scope.UpdateActivations = function() {
      for (i in $scope.sentences) {
        if (!$scope.sentences[i].determined) {
          $scope.sentences[i].activated = ($scope.sentences[i].word_len >= $scope.min_sent_len);
        }
      }
      $scope.UpdateRequest();
      $scope.UpdateTotalPrice();
    };


    $scope.UpdateRequest = function() {
      var activated_list = [];
      for (i in $scope.sentences) {
        if ($scope.sentences[i].activated) {
          activated_list.push(i);
        }
      }
      $scope.request = JSON.stringify(activated_list);
    }

    $scope.ProceedToSecondStep = function(call_when_done) {
      if (call_when_done == null) {
        call_when_done = function() {};
      }
      $scope.not_loading = false;

      WordContentPreprocessing.Preproc(
        $scope.content,
        function(resp) {
	  resp = resp.data;

          $scope.not_loading = true;
          $scope.second_step_not_ready = false;
          $scope.paragraphs = resp.paragraphs;
          $scope.paragraphs_json = JSON.stringify(resp);
          $scope.sentences = [];
          for (var i in $scope.paragraphs) {
            $scope.sentences.push.apply(
              $scope.sentences,
              $scope.paragraphs[i].sentences);
          }
          for (var i in $scope.sentences) {
            $scope.sentences[i].word_len = $scope.sentences[i].words.length;
            $scope.sentences[i].activated = true;
            $scope.sentences[i].determined = false;
            $scope.sentences[i].ind = i;
          }
          $scope.UpdateActivations();
          $scope.UpdateRequest();
	  
          var url = location.href;
          location.href = "#picker_anchor";
          history.replaceState(null, null, url);
	  
          call_when_done();
        }
      );
    };

    $scope.BackToStepOne = function() {
      $scope.second_step_not_ready = true;
      $scope.paragraphs = [];
      $scope.sentences = [];
    };
  }
  $window.init = function() {
    $scope.$apply($scope.init_func);
  };
});
