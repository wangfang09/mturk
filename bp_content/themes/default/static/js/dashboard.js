function init() {
  window.setTimeout(window.init, 100);
}

var dashboard_app = angular.module(
  'DashboardApp', ['ui.bootstrap']);

dashboard_app.config(
  ['$interpolateProvider', function($interpolateProvider) {
    $interpolateProvider.startSymbol('{[');
    $interpolateProvider.endSymbol(']}');
  }]);


dashboard_app.factory('DashboardDataFetching', function($location, $http) {
  return {
    'GetArticleProgress': function(article_urlsafe, call_back) {
      $http.post("/_api/get_progress", JSON.stringify({
        'article_urlsafe': article_urlsafe
      })).then(call_back);
    },
  }
});


dashboard_app.controller('DashboardCtrl', function($scope, $window, DashboardDataFetching) {
  $scope.init_func = function() {
    $scope.data_ready_map = {};
    $scope.all_finished_map = {};
    $scope.n_finished_map = {};
    $scope.n_requested_map = {};

    // Generating a callback function that updates the data for an article
    UpdateDataFuncForArticle = function(urlsafe) {
      return function(resp) {
        $scope.data_ready_map[urlsafe] = true;
        $scope.n_finished_map[urlsafe] = resp.data.n_finished;
        $scope.n_requested_map[urlsafe] = resp.data.n_total;
        $scope.all_finished_map[urlsafe] = (resp.data.n_finished == resp.data.n_total);
      }
    };

    for (var i in data_retrieval_list) {
      var urlsafe = data_retrieval_list[i];
      console.log(urlsafe);
      $scope.data_ready_map[urlsafe] = true;
      console.log($scope.data_ready_map[urlsafe]);
      DashboardDataFetching.GetArticleProgress(urlsafe, UpdateDataFuncForArticle(urlsafe));
      console.log("Done");
    }
  }
  $window.init = function() {
    $scope.$apply($scope.init_func);
  };
});
