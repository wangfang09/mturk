function init() {
  window.setTimeout(window.init, 100);
}

var add_credits_app = angular.module(
  'AddCreditsApp', ['ui.bootstrap']);

add_credits_app.config(
  ['$interpolateProvider', function($interpolateProvider) {
    $interpolateProvider.startSymbol('{[');
    $interpolateProvider.endSymbol(']}');
  }]);

add_credits_app.controller('AddCreditsCtrl', function($scope, $window) {
  $scope.init_func = function() {
    $scope.total_credits = "0";
    $scope.subtotal = 0.00;
    $scope.total_price = 0.00;

    $scope.UpdatePrice = function() {
      $scope.subtotal = $scope.total_credits / 100.0;
      $scope.total_price = $scope.subtotal + 0.3;
    }
  }
  $window.init = function() {
    $scope.$apply($scope.init_func);
  };
});