var preview_request_app = angular.module(
  'PreviewRequestApp', []);

preview_request_app.config(
  ['$interpolateProvider', function($interpolateProvider) {
    $interpolateProvider.startSymbol('{[');
    $interpolateProvider.endSymbol(']}');
  }]);

preview_request_app.controller('RequestPreviewCtrl', function($scope) {
  // $scope.sentences = [
  // 	{'words' : ['I', 'think', 'this', 'looks', 'good', '.'],
  // 	 'activated' : true,
  // 	 'word_len' : 5,
  // 	 'determined' : false,
  // 	},
  // 	{'words' : ['Too', 'short', '.'],
  // 	 'activated' : true,
  // 	 'word_len' : 2,
  // 	 'determined' : false,
  // 	},
  // ];
  $scope.paragraphs = JSON.parse(
    document.getElementById('sent_data').innerHTML);
  $scope.sentences = [];
  for (var i in $scope.paragraphs) {
    $scope.sentences.push.apply(
      $scope.sentences, $scope.paragraphs[i]);
  }
  for (var i in $scope.sentences) {
    $scope.sentences[i].activated = true;
    $scope.sentences[i].determined = false;
  }

  $scope.ToggleActivation = function(ind) {
    $scope.sentences[ind].activated = !$scope.sentences[ind].activated;
    $scope.sentences[ind].determined = true;
    $scope.UpdateTotalPrice();
  };

  $scope.CalculateTotalPrice = function() {
    var total_activated = 0;
    for (i in $scope.sentences) {
      if ($scope.sentences[i].activated) {
        total_activated += 1;
      }
    }
    return Math.max(
      1.0, (parseFloat($scope.price) + 0.01) * total_activated + 0.30);
  };

  $scope.UpdateActivations = function() {
    for (i in $scope.sentences) {
      if (!$scope.sentences[i].determined) {
        $scope.sentences[i].activated = ($scope.sentences[i].word_len >= $scope.min_sent_len);
      }
    }
    $scope.UpdateTotalPrice();
  };

  $scope.UpdateTotalPrice = function() {
    var total_price = $scope.CalculateTotalPrice();
    $scope.total_price = total_price.toFixed(2);
  };

  $scope.UpdateRequest = function() {
    var activated_list = [];
    for (i in $scope.sentences) {
      if ($scope.sentences[i].activated) {
        activated_list.push(i);
      }
    }
    $scope.request = JSON.stringify(activated_list);
  }

  $scope.request = null;
  $scope.min_sent_len = 1;
  $scope.price = 0.02;
  $scope.total_price = 0.0;

  $scope.UpdateActivations();
  $scope.UpdateRequest();
  console.log($scope.sentences);
});