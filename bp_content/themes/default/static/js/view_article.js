function init() {
  window.setTimeout(window.init, 100);
}

var viewarticle_app = angular.module(
  'ViewArticleApp', ['ui.bootstrap']);

viewarticle_app.config(
  ['$interpolateProvider', function($interpolateProvider) {
    $interpolateProvider.startSymbol('{[');
    $interpolateProvider.endSymbol(']}');
  }]);


viewarticle_app.factory('ViewArticleDataFetching', function($location, $http) {
  return {
    'GetRevisionResult': function(article_urlsafe, call_back) {
      $http.post("/_api/get_result", JSON.stringify({
        'article_urlsafe': article_urlsafe
      })).then(call_back);
    },
    'GetArticleURLSafe': function() {
      return article_urlsafe;	// This is the variable declared in the article's HTML
    }
  }
});


viewarticle_app.controller('ViewArticleCtrl', function($scope, $window, ViewArticleDataFetching) {
  $scope.revision_result = null;
  $scope.loading_finished = false;
  $scope.viewing_mode = 'Edits';

  $scope.style_map = {
    'Original': {
      'orig': '',
      'revised': 'hidden'
    },
    'Result': {
      'orig': 'hidden',
      'revised': ''
    },
    'Edits': {
      'orig': 'crossed-out',
      'revised': 'correction'
    },
  };

  // Generating a callback function that updates the data for an article
  UpdateDataFuncForArticle = function(urlsafe) {
    return function(resp) {
      $scope.revision_result = resp.data;
      $scope.loading_finished = true;
    }
  };


  $scope.init_func = function() {
    article_urlsafe = ViewArticleDataFetching.GetArticleURLSafe();
    ViewArticleDataFetching.GetRevisionResult(
      article_urlsafe, UpdateDataFuncForArticle(article_urlsafe));
  }

  $window.init = function() {
    $scope.$apply($scope.init_func);
  };
});
