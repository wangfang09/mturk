# -*- coding: utf-8 -*-
from bp_includes.lib.basehandler import BaseHandler
from google.appengine.api import taskqueue
from google.appengine.api import users
from google.appengine.ext import ndb
from bp_content.themes.default import models
from bp_content.themes.default import turk


class AdminLogoutHandler(BaseHandler):

  def get(self):
    self.redirect(users.create_logout_url(dest_url=self.uri_for('home')))


class ViewRequestsHandler(BaseHandler):

  def get(self):
    articles = models.AnnotationRequest.query().order(
      -models.AnnotationRequest.creation_timestamp)

    params = {
      "articles": articles,
    }
    self.render_template("admin_list_articles.html", **params)


class ViewArticleHandler(BaseHandler):

  def get(self, article_urlsafe):
    article = ndb.Key(urlsafe=article_urlsafe).get()
    self.render_template(
      "admin_article.html",
      article=article.GetRenderingData(), key=article.key)


class AmtDashboardHandler(BaseHandler):

  def get(self):
    params = {
      "balance_amount": turk.TurkConfig.GetTheConfig().AccountBalance(),
    }
    self.render_template("admin_amt_dashboard.html", **params)


class MarkAsPaidHandler(BaseHandler):

  def get(self, order_urlsafe):
    taskqueue.add(url="/taskqueue_mark_order_as_paid/",
                  params={"order_key": order_urlsafe})


class ExpireArticleHandler(BaseHandler):

  def get(self, article_urlsafe):
    article = ndb.Key(urlsafe=article_urlsafe).get()
    article.CancelIssuedRequests()
    self.redirect(
      self.uri_for(
        'admin-request-view',
        article_urlsafe=article_urlsafe))
