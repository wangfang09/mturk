# NLP server for all NLP related backend processing

By default I run it at sword.xuehuichao.com:8085.

```sh
$ docker build -t nlp_server .
$ docker run -d -p 8085:8085 --name nlp_server nlp_server
$ docker start nlp_server
```

### API
#### Correction Detection
Compare two sentences with a JSON-RPC request. The server will respond with invidual corrections (e.g. <del>error</del> *into* errors), and their types (e.g. spelling error).
```sh
$ curl --data-binary '{"params" : ["This sentence might have contain error.", "This sentence might have some errors."], "id" : 0, "jsonrpc" : "2.0", "method" : "CorrDet"}' -H 'content-type:text/plain;' http://127.0.0.1:8085

{"jsonrpc": "2.0", "result": [["This sentence might have", null, null], ["contain", "some", "needs replacing"], ["error", "errors", "wrong noun form"], [".", null, null]], "id": 0}
```
