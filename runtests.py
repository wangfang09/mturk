#!/usr/bin/python

import optparse
import os
import sys
import unittest
import logging
from nose.plugins import multiprocess

USAGE = """%prog SDK_PATH TEST_PATH
Run unit tests for App Engine apps.

SDK_PATH    Path to Google Cloud or Google App Engine SDK installation, usually
            ~/google_cloud_sdk
TEST_PATH   Path to package containing test modules"""


def main(sdk_path, test_path, verbosity):
  # If the sdk path points to a google cloud sdk installation
  # then we should alter it to point to the GAE platform location.
  sys.path.insert(
    0,
    os.path.join(
      os.path.dirname(__file__),
      'bp_includes/external'))
  if os.path.exists(os.path.join(sdk_path, 'platform/google_appengine')):
    sys.path.insert(0, os.path.join(sdk_path, 'platform/google_appengine'))
  else:
    sys.path.insert(0, sdk_path)
  sys.path.insert(0, os.path.join(os.path.dirname(__file__)))

  # Ensure that the google.appengine.* packages are available
  # in tests as well as all bundled third-party packages.
  import dev_appserver
  dev_appserver.fix_sys_path()

  # Loading appengine_config from the current project ensures that any
  # changes to configuration there are available to all tests (e.g.
  # sys.path modifications, namespaces, etc.)
  try:
    import appengine_config
    (appengine_config)
  except ImportError:
    print 'Note: unable to import appengine_config.'

  logger_level = logging.FATAL
  if verbosity == 'normal':
    if test_path != 'all':
      logger_level = logging.DEBUG
  elif verbosity == 'verbose':
    logger_level = logging.DEBUG
  else:
    raise ValueError('invalid verbosity mode: %s'.format(verbosity))

  # Discover and run tests.
  if test_path == 'all':
    logger = logging.getLogger()
    logger.setLevel(logger_level)
    suite = unittest.loader.TestLoader().discover('./')
  else:
    logger = logging.getLogger()
    logger.setLevel(logger_level)
    suite = unittest.loader.TestLoader().loadTestsFromName(test_path)

  unittest.TextTestRunner(verbosity=1).run(suite)


if __name__ == '__main__':
  SDK_PATH = '/usr/local/google_appengine'
  assert len(sys.argv) == 3
  TEST_PATH = sys.argv[1]
  verbose_mode = sys.argv[2]
  main(SDK_PATH, TEST_PATH, verbose_mode)
