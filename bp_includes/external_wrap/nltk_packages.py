import sys
import os
import logging

EXTERNAL_PACAKGES_PATH = os.path.join(
  os.path.dirname(__file__), '..', '..', 'bp_includes/external')

logging.info("Added dependency on external path: %s", EXTERNAL_PACAKGES_PATH)
sys.path.insert(0, EXTERNAL_PACAKGES_PATH)

import nltk

nltk.data.path.append("nltkdata")
from nltk.tokenize import sent_tokenize, word_tokenize
