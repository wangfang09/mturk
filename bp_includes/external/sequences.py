class AnnotatableSequence(object):

  def __init__(self, sequence):
    self.sequence = sequence
    self.annotations = []
    self.alarm = True

  def Annotate(self, range_start, range_end, tag):
    if range_start > range_end:
      if self.alarm:
        raise ValueError
      else:
        return
    for start, end, symbol in self.annotations:
      if start <= range_start < end or start < range_end < end:
        if self.alarm:
          raise ValueError("Overlap in sequence: %r and %r" % (
            (start, end), (range_start, range_end)))
        else:
          return
    if range_start > len(self.sequence) or range_end < 0:
      if self.alarm:
        raise ValueError("The new annotation is out of range %r" % (
          (range_start, range_end),))
      else:
        return
    self.annotations.append((range_start, range_end, tag))
    
  """def Annotate(self,tag):
    self.annotations.append(tag)"""

  def GetSegments(self):
    self.annotations.sort()
    segments = []

    prev_end = 0
    for i in range(len(self.annotations)):
      start, end, tag = self.annotations[i]
      if start != prev_end:
        assert start > prev_end
        segments.append((self.sequence[prev_end:start], None))
      segments.append((self.sequence[start:end], tag))
      prev_end = end
    if prev_end < len(self.sequence):
      segments.append((self.sequence[prev_end:], None))

    return segments

  def SetOverlapAlarm(self, alarm_on):
    self.alarm = alarm_on
